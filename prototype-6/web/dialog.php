<?php
/* Copyright (C) 2019-2023 Stephan Kreutzer
 *
 * This file is part of GBA.
 *
 * GBA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * GBA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with GBA. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/dialog.php
 * @author Stephan Kreutzer
 * @since 2019-09-09
 */



require_once("./libraries/https.inc.php");
require_once("./libraries/session.inc.php");
require_once("./libraries/user_defines.inc.php");
require_once("./libraries/database.inc.php");

if ($_SESSION['user_role'] !== USER_ROLE_ADMIN)
{
    http_response_code(403);
    exit(0);
}

echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n".
     "<!DOCTYPE html\n".
     "    PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"\n".
     "    \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n".
     "<html version=\"-//W3C//DTD XHTML 1.1//EN\" xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.w3.org/1999/xhtml http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd\" xml:lang=\"en\" lang=\"en\">\n".
     "  <head>\n".
     "    <meta http-equiv=\"content-type\" content=\"application/xhtml+xml; charset=UTF-8\"/>\n".
     "    <title>Dialog</title>\n".
     "    <style type=\"text/css\">\n".
     "      body\n".
     "      {\n".
     "          font-family: monospace;\n".
     "      }\n".
     "    </style>\n".
     "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>\n".
     "  </head>\n".
     "  <body>\n".
     "    <div>\n".
     "      <h2>Dialog</h2>\n";

if (isset($_POST['delete']) === true)
{
    if (isset($_POST['id']) === true)
    {
        $result = Database::Get()->Execute("DELETE FROM `".Database::Get()->GetPrefix()."dialogs`\n".
                                           "WHERE `id`=?",
                                           array($_POST['id']),
                                           array(Database::TYPE_INT));

        if ($result !== true)
        {
            
        }
    }
}
else if (isset($_POST['add']) === true)
{
    if (isset($_POST['id']) === true &&
        isset($_POST['initiator_id']) === true &&
        isset($_POST['initiator_title']) === true &&
        isset($_POST['participant_id']) === true &&
        isset($_POST['participant_title']) === true)
    {
        $idValue = NULL;
        $idType = Database::TYPE_NULL;

        if (is_numeric($_POST['id']) === true)
        {
            $idValue = (int)$_POST['id'];
            $idType = Database::TYPE_INT;
        }

        $result = Database::Get()->Insert("INSERT INTO `".Database::Get()->GetPrefix()."dialogs` (`id`,\n".
                                          "    `id_users_initiator`,\n".
                                          "    `initiator_title`,\n".
                                          "    `initiator_mode`,\n".
                                          "    `id_entries_initiator_entry_last`,\n".
                                          "    `initiator_last_action`,\n".
                                          "    `id_users_participant`,\n".
                                          "    `participant_title`,\n".
                                          "    `participant_mode`,\n".
                                          "    `id_entries_participant_entry_last`,\n".
                                          "    `participant_last_action`)\n".
                                          "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                                          array($idValue, $_POST['initiator_id'], $_POST['initiator_title'], 0, 0, NULL, $_POST['participant_id'], $_POST['participant_title'], 0, 0, NULL),
                                          array($idType, Database::TYPE_INT, Database::TYPE_STRING, Database::TYPE_INT, Database::TYPE_INT, Database::TYPE_NULL, Database::TYPE_INT, Database::TYPE_STRING, Database::TYPE_INT, Database::TYPE_INT, Database::TYPE_NULL));

        if ($result <= 0)
        {
            
        }
    }
}

$dialogs = Database::Get()->QueryUnsecure("SELECT `id`,\n".
                                          "    `id_users_initiator`,\n".
                                          "    `initiator_title`,\n".
                                          "    `id_users_participant`,\n".
                                          "    `participant_title`\n".
                                          "FROM `".Database::Get()->GetPrefix()."dialogs`\n".
                                          "WHERE 1\n".
                                          "ORDER BY `id` ASC");

if (is_array($dialogs) !== true)
{
    http_response_code(500);
    exit(-1);
}

echo  "      <table border=\"1\">\n".
      "        <thead>\n".
      "          <tr>\n".
      "            <th>ID</th>\n".
      "            <th>Initiator</th>\n".
      "            <th>Initiator Title</th>\n".
      "            <th>Participant</th>\n".
      "            <th>Participant Title</th>\n".
      "            <th>Action</th>\n".
      "          </tr>\n".
      "        </thead>\n".
      "        <tbody>\n";

if (count($dialogs) > 0)
{
    foreach ($dialogs as $dialog)
    {
        echo "          <tr>\n".
             "            <td>".((int)$dialog['id'])."</td>\n".
             "            <td>".((int)$dialog['id_users_initiator'])."</td>\n".
             "            <td>".htmlspecialchars($dialog['initiator_title'], ENT_XHTML, "UTF-8")."</td>\n".
             "            <td>".htmlspecialchars($dialog['id_users_participant'], ENT_XHTML, "UTF-8")."</td>\n".
             "            <td>".htmlspecialchars($dialog['participant_title'], ENT_XHTML, "UTF-8")."</td>\n".
             "            <td>\n".
             "              <form action=\"dialog.php\" method=\"post\">\n".
             "                <fieldset>\n".
             "                  <input type=\"submit\" name=\"delete\" value=\"Delete\"/>\n".
             "                  <input type=\"hidden\" name=\"id\" value=\"".((int)$dialog['id'])."\"/>\n".
             "                </fieldset>\n".
             "              </form>\n".
             "            </td>\n".
             "          </tr>\n";
    }
}

echo "        </tbody>\n".
     "      </table>\n".
     "      <form action=\"dialog.php\" method=\"post\">\n".
     "        <fieldset>\n".
     "          <input type=\"text\" name=\"id\" size=\"20\" maxlength=\"60\"/> ID dialog<br/>\n".
     "          <input type=\"text\" name=\"initiator_id\" size=\"20\" maxlength=\"60\"/> ID initiator<br/>\n".
     "          <input type=\"text\" name=\"initiator_title\" size=\"20\" maxlength=\"60\"/> Title initiator<br/>\n".
     "          <input type=\"text\" name=\"participant_id\" size=\"20\" maxlength=\"60\"/> ID participant<br/>\n".
     "          <input type=\"text\" name=\"participant_title\" size=\"20\" maxlength=\"60\"/> Title participant<br/>\n".
     "          <input type=\"submit\" name=\"add\" value=\"Add\"/>\n".
     "        </fieldset>\n".
     "      </form>\n".
     "    </div>\n".
     "    <div>\n".
     "      <a href=\"index.php\">Main</a>\n".
     "    </div>\n".
     "  </body>\n".
     "</html>\n".
     "\n";


?>
