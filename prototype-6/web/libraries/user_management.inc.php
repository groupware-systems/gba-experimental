<?php
/* Copyright (C) 2012-2019  Stephan Kreutzer
 *
 * This file is part of GBA.
 *
 * GBA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * GBA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with GBA. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/libraries/user_management.inc.php
 * @author Stephan Kreutzer
 * @since 2012-06-02
 */



require_once(dirname(__FILE__)."/database.inc.php");
require_once(dirname(__FILE__)."/user_defines.inc.php");



function insertNewUser($name, $password, $email, $role)
{
    /** @todo Check for empty $name, $password, $email or $role. */

    if (Database::Get()->IsConnected() !== true)
    {
        return -1;
    }

    if (Database::Get()->BeginTransaction() !== true)
    {
        return -2;
    }

    $salt = md5(uniqid(rand(), true));
    $password = hash('sha512', $salt.$password);

    $id = Database::Get()->Insert("INSERT INTO `".Database::Get()->GetPrefix()."users` (`id`,\n".
                                  "    `name`,\n".
                                  "    `e_mail`,\n".
                                  "    `salt`,\n".
                                  "    `password`,\n".
                                  "    `role`)\n".
                                  "VALUES (?, ?, ?, ?, ?, ?)\n",
                                  array(NULL, $name, $email, $salt, $password, $role),
                                  array(Database::TYPE_NULL, Database::TYPE_STRING, Database::TYPE_STRING, Database::TYPE_STRING, Database::TYPE_STRING, Database::TYPE_INT));

    if ($id <= 0)
    {
        Database::Get()->RollbackTransaction();
        return -4;
    }

    /*
    @mail("recipient@example.org",
          "[GBA] New user registered",
          "Time: ".date("c")."\n".
          "User: ".htmlspecialchars($name, ENT_XHTML, "UTF-8")."\n".
          "From: noreply@example.org\n".
          "MIME-Version: 1.0\n".
          "Content-type: text/plain; charset=UTF-8\n");

    $idDialog = Database::Get()->Insert("INSERT INTO `".Database::Get()->GetPrefix()."dialogs` (`id`,\n".
                                        "    `id_users_initiator`,\n".
                                        "    `initiator_title`,\n".
                                        "    `initiator_mode`,\n".
                                        "    `id_entries_initiator_entry_last`,\n".
                                        "    `initiator_last_action`,\n".
                                        "    `id_users_participant`,\n".
                                        "    `participant_title`,\n".
                                        "    `participant_mode`,\n".
                                        "    `id_entries_participant_entry_last`,\n".
                                        "    `participant_last_action`)\n".
                                        "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                                        array(NULL, 1, $name, 0, 0, NULL, $id, "Trail 1", 0, 0, NULL),
                                        array(Database::TYPE_NULL, Database::TYPE_INT, Database::TYPE_STRING, Database::TYPE_INT, Database::TYPE_INT, Database::TYPE_NULL, Database::TYPE_INT, Database::TYPE_STRING, Database::TYPE_INT, Database::TYPE_INT, Database::TYPE_NULL));

    if ($idDialog <= 0)
    {
        Database::Get()->RollbackTransaction();
        return -8;
    }

    $idEntry = Database::Get()->Insert("INSERT INTO `".Database::Get()->GetPrefix()."entries` (`id`,\n".
                                       "    `id_entries`,\n".
                                       "    `id_dialogs`)\n".
                                       "VALUES (NULL, NULL, ?)",
                                       array($idDialog),
                                       array(Database::TYPE_INT));

    if ($idEntry <= 0)
    {
        Database::Get()->RollbackTransaction();
        return -9;
    }

    $idEntryRevision = Database::Get()->Insert("INSERT INTO `".Database::Get()->GetPrefix()."entry_revisions` (`id`,\n".
                                               "    `text`,\n".
                                               "    `revision_datetime`,\n".
                                               "    `id_users`,\n".
                                               "    `id_entries`)\n".
                                               "VALUES (?, ?, UTC_TIMESTAMP(), ?, ?)",
                                               array(NULL, "Hi there!", 1, $idEntry),
                                               array(Database::TYPE_NULL, Database::TYPE_STRING, Database::TYPE_INT, Database::TYPE_INT));

    if ($idEntryRevision <= 0)
    {
        Database::Get()->RollbackTransaction();
        return -10;
    }
    */

    if (Database::Get()->CommitTransaction() === true)
    {
        return $id;
    }

    return -7;
}

function getUserByName($name)
{
    /** @todo Check for empty $name, $password, $email or $role. */

    if (Database::Get()->IsConnected() !== true)
    {
        return -1;
    }

    $user = Database::Get()->Query("SELECT `id`,\n".
                                   "    `salt`,\n".
                                   "    `password`,\n".
                                   "    `role`\n".
                                   "FROM `".Database::Get()->GetPrefix()."users`\n".
                                   "WHERE `name` LIKE ?\n",
                                   array($name),
                                   array(Database::TYPE_STRING));

    if (is_array($user) !== true)
    {
        return -2;
    }

    return $user;
}

function GetUsers()
{
    if (Database::Get()->IsConnected() !== true)
    {
        return -1;
    }

    $users = Database::Get()->QueryUnsecure("SELECT `id`,\n".
                                            "    `name`,\n".
                                            "    `role`\n".
                                            "FROM `".Database::Get()->GetPrefix()."users`\n".
                                            "WHERE 1\n");

    if (is_array($users) !== true)
    {
        return -2;
    }

    return $users;
}



?>
