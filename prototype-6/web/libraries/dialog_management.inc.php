<?php
/* Copyright (C) 2019  Stephan Kreutzer
 *
 * This file is part of GBA.
 *
 * GBA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * GBA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with GBA. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/libraries/dialog_management.inc.php
 * @author Stephan Kreutzer
 * @since 2019-09-06
 */



require_once(dirname(__FILE__)."/database.inc.php");
//require_once(dirname(__FILE__)."/user_defines.inc.php");



define("ERRORCODE_DIALOGMANAGEMENT_GETDIALOGBYID_NORESULTS", -3);

// This could check for the user ID as well, but if no entries
// are found with the dialog ID and user ID, then it's not really
// clear if that's because of a non-existent dialog ID or a mismatch
// for the user ID...
function GetDialogById($id)
{
    if (Database::Get()->IsConnected() !== true)
    {
        return -1;
    }

    $dialog = Database::Get()->Query("SELECT `id_users_initiator`,\n".
                                     "    `initiator_title`,\n".
                                     "    `initiator_mode`,\n".
                                     "    `id_entries_initiator_entry_last`,\n".
                                     "    `initiator_last_action`,\n".
                                     "    `id_users_participant`,\n".
                                     "    `participant_title`,\n".
                                     "    `participant_mode`,\n".
                                     "    `id_entries_participant_entry_last`,\n".
                                     "    `participant_last_action`\n".
                                     "FROM `".Database::Get()->GetPrefix()."dialogs`\n".
                                     "WHERE `id`=?\n",
                                     array($id),
                                     array(Database::TYPE_INT));

    if (is_array($dialog) !== true)
    {
        return -2;
    }

    if (count($dialog) <= 0)
    {
        return ERRORCODE_DIALOGMANAGEMENT_GETDIALOGBYID_NORESULTS;
    }

    return $dialog[0];
}

function GetDialogsByUserId($idUser)
{
    if (Database::Get()->IsConnected() !== true)
    {
        return -1;
    }

    $dialogs = Database::Get()->Query("SELECT `id`,\n".
                                      "    `id_users_initiator`,\n".
                                      "    `initiator_title`,\n".
                                      "    `initiator_mode`,\n".
                                      "    `id_entries_initiator_entry_last`,\n".
                                      "    `initiator_last_action`,\n".
                                      "    `id_users_participant`,\n".
                                      "    `participant_title`,\n".
                                      "    `participant_mode`,\n".
                                      "    `id_entries_participant_entry_last`,\n".
                                      "    `participant_last_action`\n".
                                      "FROM `".Database::Get()->GetPrefix()."dialogs`\n".
                                      "WHERE `id_users_initiator`=? OR\n".
                                      "    `id_users_participant`=?\n".
                                      "ORDER BY `id` ASC",
                                      array($idUser, $idUser),
                                      array(Database::TYPE_INT, Database::TYPE_INT));

    if (is_array($dialogs) !== true)
    {
        return -2;
    }

    return $dialogs;
}



?>
