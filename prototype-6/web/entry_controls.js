/* Copyright (C) 2019-2023 Stephan Kreutzer
 *
 * This file is part of GBA.
 *
 * GBA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * GBA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with GBA. If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

function LoadInputControl(idEntry, idDialog)
{
    let updateControlDiv = document.getElementById('update_control');
    let entryTextSpan = document.getElementById("entry_text");

    if (updateControlDiv == null ||
        entryTextSpan == null)
    {
        return -1;
    }

    for (let i = updateControlDiv.childNodes.length - 1; i >= 0; i--)
    {
        removeNode(updateControlDiv.childNodes[i], updateControlDiv);
    }

    if (idDialog > 0 &&
        idEntry > 0)
    {
        let form = document.createElement("form");
        form.setAttribute("method", "post");
        form.setAttribute("action", "entry.php?dialog=" + idDialog + "&id=" + idEntry);

        let fieldset = document.createElement("fieldset");

        let textarea = document.createElement("textarea");
        textarea.setAttribute("name", "text");
        textarea.setAttribute("rows", "24");
        textarea.setAttribute("cols", "80");

        {
            let textareaText = document.createTextNode(entryTextSpan.innerText);
            textarea.appendChild(textareaText);
        }

        let submit = document.createElement("input");
        submit.setAttribute("type", "submit");
        submit.setAttribute("name", "submit");
        submit.setAttribute("value", "Update");

        let cancel = document.createElement("input");
        cancel.setAttribute("type", "button");
        cancel.setAttribute("onclick", "LoadInputControl(0, 0);");
        cancel.setAttribute("value", "Cancel");

        fieldset.appendChild(textarea);
        fieldset.appendChild(submit);
        fieldset.appendChild(cancel);

        form.appendChild(fieldset);
        updateControlDiv.appendChild(form);
    }

    return 0;
}

function ToggleRevisions()
{
    let revisionsDiv = document.getElementById("revisions");

    if (revisionsDiv == null)
    {
        return -1;
    }

    if (revisionsDiv.style.display === "block")
    {
        revisionsDiv.style.display = "none";
    }
    else
    {
        revisionsDiv.style.display = "block";
    }

    return 0;
}

// Stupid JavaScript has a cloneNode(deep), but no removeNode(deep).
function removeNode(element, parent)
{
    // TODO: Reverse delete for performance.
    while (element.hasChildNodes() == true)
    {
        removeNode(element.lastChild, element);
    }

    parent.removeChild(element);
}
