<?php
/* Copyright (C) 2012-2023 Stephan Kreutzer
 *
 * This file is part of GBA.
 *
 * GBA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * GBA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with GBA. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/answers.php
 * @author Stephan Kreutzer
 * @since 2019-07-31
 */



if (isset($_GET['id_question']) !== true)
{
    http_response_code(400);
    exit(-1);
}

$idQuestion = (int)$_GET['id_question'];

require_once("./libraries/database.inc.php");

if (Database::Get()->IsConnected() !== true)
{
    http_response_code(500);
    exit(-1);
}

$question = Database::Get()->Query("SELECT `title`,\n".
                                   "    `id_missions`\n".
                                   "FROM `".Database::Get()->GetPrefix()."questions`\n".
                                   "WHERE `id`=?\n",
                                   array($idQuestion),
                                   array(Database::TYPE_INT));

if (is_array($question) !== true)
{
    http_response_code(500);
    exit(-1);
}

if (count($question) <= 0)
{
    http_response_code(404);
    exit(-1);
}

$question = $question[0];


echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n".
     "<!DOCTYPE html\n".
     "    PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"\n".
     "    \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n".
     "<html version=\"-//W3C//DTD XHTML 1.1//EN\" xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.w3.org/1999/xhtml http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd\" xml:lang=\"en\" lang=\"en\">\n".
     "  <head>\n".
     "    <meta http-equiv=\"content-type\" content=\"application/xhtml+xml; charset=UTF-8\"/>\n".
     "    <title>Answers</title>\n".
     "    <style type=\"text/css\">\n".
     "      .deleted\n".
     "      {\n".
     "          display: none;\n".
     "      }\n".
     "    </style>\n".
     "    <script type=\"text/javascript\" src=\"answers_controls.js\"></script>\n".
     "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>\n".
     "  </head>\n".
     "  <body onload=\"LoadInputControl(".$idQuestion.", 0);\">\n".
     "    <div>\n".
     "      <h2>Answers</h2>\n".
     "      <div>\n".
     "        Question: ".$question['title']."\n".
     "      </div>\n";

$errors = "";

if (isset($_POST['text']) === true)
{
    if (strlen($_POST['text']) <= 0 &&
        isset($_POST['id_answer_edit']) != true)
    {
        $errors .= "<p>Error: Input field empty.</p>";
    }

    if (strlen($errors) <= 0)
    {
        if (Database::Get()->IsConnected() !== true)
        {
            $errors .= "<p>Error: Database isn't connected.</p>";
        }
    }

    if (strlen($errors) <= 0)
    {
        if (Database::Get()->BeginTransaction() !== true)
        {
            $errors .= "<p>Error: Can't begin database transaction.</p>";
        }
    }

    $idAnswer = -1;
    $idAnswerRevision = -1;

    if (strlen($errors) <= 0)
    {
        if (isset($_POST['id_answer_edit']) === true)
        {
            $idAnswer = (int)$_POST['id_answer_edit'];

            $answer = Database::Get()->Query("SELECT `id`\n".
                                             "FROM `".Database::Get()->GetPrefix()."answers`\n".
                                             "WHERE `id`=? AND\n".
                                             "    `id_questions`=?",
                                             array($idAnswer, $idQuestion),
                                             array(Database::TYPE_INT, Database::TYPE_INT));

            if (is_array($answer) !== true)
            {
                $errors .= "<p>Error: Failed to determine from the database if the requested answer belongs to the question.</p>";
                $idAnswer = -1;
            }

            if (strlen($errors) <= 0)
            {
                if (count($answer) <= 0)
                {
                    $errors .= "<p>Error: Couldn't verify if the requested answer belongs to the question.</p>";
                    $idAnswer = -1;
                }
            }
        }
        else
        {
            $idAnswer = Database::Get()->Insert("INSERT INTO `".Database::Get()->GetPrefix()."answers` (`id`,\n".
                                                "    `id_questions`)\n".
                                                "VALUES (?, ?)",
                                                array(NULL, $idQuestion),
                                                array(Database::TYPE_NULL, Database::TYPE_INT));

            if ($idAnswer <= 0)
            {
                Database::Get()->RollbackTransaction();

                $errors .= "<p>Error: Insertion failed.</p>";
            }
        }
    }

    if (strlen($errors) <= 0)
    {
        $idAnswerRevision = Database::Get()->Insert("INSERT INTO `".Database::Get()->GetPrefix()."answer_revisions` (`id`,\n".
                                                    "    `text`,\n".
                                                    "    `revision_datetime`,\n".
                                                    "    `id_answers`)\n".
                                                    "VALUES (?, ?, UTC_TIMESTAMP(), ?)",
                                                    array(NULL, $_POST['text'], $idAnswer),
                                                    array(Database::TYPE_NULL, Database::TYPE_STRING, Database::TYPE_INT));

        if ($idAnswerRevision <= 0)
        {
            Database::Get()->RollbackTransaction();

            $errors .= "<p>Error: Insertion failed.</p>";
        }
    }

    if (strlen($errors) <= 0)
    {
        if (Database::Get()->CommitTransaction() !== true)
        {
            $errors .= "<p>Error: Committing the transaction failed.</p>";
        }
    }
}

if (strlen($errors) > 0)
{
    echo "      <div>\n".
         "        ".$errors."\n".
         "      </div>\n";

    $errors = "";
}

echo "      <div id=\"input_control\"></div>\n";

$answers = array();

if (strlen($errors) <= 0)
{
    if (Database::Get()->IsConnected() !== true)
    {
        $errors .= "<p>Error: Database isn't connected.</p>";
    }

    if (strlen($errors) <= 0)
    {
        $answers = Database::Get()->Query("SELECT `".Database::Get()->GetPrefix()."answers`.`id` AS `answers_id`,\n".
                                          "    `".Database::Get()->GetPrefix()."answer_revisions`.`id` AS `answer_revisions_id`,\n".
                                          "    `".Database::Get()->GetPrefix()."answer_revisions`.`text`,\n".
                                          "    `".Database::Get()->GetPrefix()."answer_revisions`.`revision_datetime`\n".
                                          "FROM `".Database::Get()->GetPrefix()."answers`\n".
                                          "INNER JOIN `".Database::Get()->GetPrefix()."answer_revisions` ON\n".
                                          "    `".Database::Get()->GetPrefix()."answers`.`id` =\n".
                                          "    `".Database::Get()->GetPrefix()."answer_revisions`.`id_answers`\n".
                                          "WHERE `".Database::Get()->GetPrefix()."answers`.`id_questions`=?\n".
                                          "ORDER BY `".Database::Get()->GetPrefix()."answers`.`id` DESC,\n".
                                          "    `".Database::Get()->GetPrefix()."answer_revisions`.`revision_datetime` DESC",
                                          array($idQuestion),
                                          array(Database::TYPE_INT));

        if (is_array($answers) !== true)
        {
            $errors .= "<p>Error: Database query failed.</p>";
        }
    }
}

if (strlen($errors) <= 0)
{
    if (count($answers) > 0)
    {
        echo "      <ul>\n";

        $lastAnswerId = -1;

        foreach ($answers as $answer)
        {
            if ($lastAnswerId != (int)$answer['answers_id'])
            {
                if ($lastAnswerId >= 0)
                {
                    echo "    </tbody>\n".
                         "  </table>\n".
                         "</div>\n".
                         "        </li>\n";
                }

                $lastAnswerId = (int)$answer['answers_id'];

                $deleted = "";

                if (strlen($answer['text']) <= 0)
                {
                    $deleted = " class=\"deleted\"";
                }

                echo "        <li".$deleted.">\n".
                     "<span id=\"answer_".$lastAnswerId."\">".htmlspecialchars($answer['text'], ENT_XHTML, "UTF-8")."</span> <a href=\"#\" onclick=\"LoadInputControl(".$idQuestion.", ".$lastAnswerId."); return false;\">Edit</a> <a href=\"#\" onclick=\"ToggleRevisions('revisions_answer_".$lastAnswerId."'); return false;\">Revisions</a>\n".
                     "<div id=\"revisions_answer_".$lastAnswerId."\" style=\"display: none;\">\n".
                     "  <table border=\"1\">\n".
                     "    <thead>\n".
                     "      <tr>\n".
                     "        <th>Timestamp (UTC)</th>\n".
                     "        <th>Version</th>\n".
                     "      </tr>\n".
                     "    </thead>\n".
                     "    <tbody>\n";
            }

            echo "      <tr>\n".
                 "        <td>".str_replace(" ", "T", $answer['revision_datetime'])."Z</td>\n".
                 "        <td>".htmlspecialchars($answer['text'], ENT_XHTML, "UTF-8")."</td>\n".
                 "      </tr>\n";
        }

        echo "    </tbody>\n".
             "  </table>\n".
             "</div>\n".
             "        </li>\n".
             "      </ul>\n";
    }
}
else
{
    echo "      <div>\n".
         "        ".$errors."\n".
         "      </div>\n";

    $errors = "";
}

echo "      <div>\n".
     "        <a href=\"./questions.php?id_mission=".((int)$question['id_missions'])."\">Back to questions</a>\n".
     "      </div>\n".
     "    </div>\n".
     "  </body>\n".
     "</html>\n".
     "\n";


?>
