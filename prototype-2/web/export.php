<?php
/* Copyright (C) 2019-2023 Stephan Kreutzer
 *
 * This file is part of GBA.
 *
 * GBA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * GBA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with GBA. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/export.php
 * @author Stephan Kreutzer
 * @since 2019-08-14
 */



require_once("./libraries/database.inc.php");

if (Database::Get()->IsConnected() !== true)
{
    http_response_code(500);
    exit(-1);
}

$missions = Database::Get()->QueryUnsecure("SELECT `id`,\n".
                                           "    `title`\n".
                                           "FROM `".Database::Get()->GetPrefix()."missions`\n".
                                           "WHERE 1\n".
                                           "ORDER BY `id` DESC");

if (is_array($missions) !== true)
{
    http_response_code(500);
    exit(-1);
}

/** @todo Support HEAD and ETag (or similar). */

header("Content-Type: application/xml");

echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n".
     "<!-- This export was created by prototype-2 of gba-experimental, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/groupware-systems/gba-experimental and http://www.groupware-systems.org). -->\n".
     "<gba-data>\n".
     "  <missions>\n";

if (count($missions) > 0)
{
    foreach ($missions as $mission)
    {
        echo "    <mission>\n".
             "      <id>".((int)$mission['id'])."</id>\n".
             "      <title>".htmlspecialchars($mission['title'], ENT_XML1, "UTF-8")."</title>\n".
             "      <questions>\n";

        $questions = Database::Get()->Query("SELECT `id`,\n".
                                            "    `title`\n".
                                            "FROM `".Database::Get()->GetPrefix()."questions`\n".
                                            "WHERE `id_missions`=?\n".
                                            "ORDER BY `id` DESC",
                                            array($mission['id']),
                                            array(Database::TYPE_INT));

        if (is_array($questions) === true)
        {
            if (count($questions) > 0)
            {
                foreach ($questions as $question)
                {
                    echo "        <question>\n".
                         "          <id>".((int)$question['id'])."</id>\n".
                         "          <title>".htmlspecialchars($question['title'], ENT_XML1, "UTF-8")."</title>\n".
                         "          <answers>\n";

                    $answers = Database::Get()->Query("SELECT `".Database::Get()->GetPrefix()."answers`.`id` AS `answers_id`,\n".
                                                      "    `".Database::Get()->GetPrefix()."answer_revisions`.`id` AS `answer_revisions_id`,\n".
                                                      "    `".Database::Get()->GetPrefix()."answer_revisions`.`text`,\n".
                                                      "    `".Database::Get()->GetPrefix()."answer_revisions`.`revision_datetime`\n".
                                                      "FROM `".Database::Get()->GetPrefix()."answers`\n".
                                                      "INNER JOIN `".Database::Get()->GetPrefix()."answer_revisions` ON\n".
                                                      "    `".Database::Get()->GetPrefix()."answers`.`id` =\n".
                                                      "    `".Database::Get()->GetPrefix()."answer_revisions`.`id_answers`\n".
                                                      "WHERE `".Database::Get()->GetPrefix()."answers`.`id_questions`=?\n".
                                                      "ORDER BY `".Database::Get()->GetPrefix()."answers`.`id` DESC,\n".
                                                      "    `".Database::Get()->GetPrefix()."answer_revisions`.`revision_datetime` DESC",
                                                      array($question['id']),
                                                      array(Database::TYPE_INT));

                    if (is_array($answers) === true)
                    {
                        if (count($answers) > 0)
                        {
                            $lastAnswerId = -1;

                            foreach ($answers as $answer)
                            {
                                if ($lastAnswerId != (int)$answer['answers_id'])
                                {
                                    if ($lastAnswerId >= 0)
                                    {
                                        echo "              </revisions>\n".
                                             "            </answer>\n";
                                    }

                                    $lastAnswerId = (int)$answer['answers_id'];

                                    $deleted = "";

                                    if (strlen($answer['text']) <= 0)
                                    {
                                        $deleted = " deleted=\"true\"";
                                    }

                                    echo "            <answer".$deleted.">\n".
                                         "              <revisions>\n";
                                }

                                echo "                <revision>\n".
                                     "                  <datetime>".str_replace(" ", "T", $answer['revision_datetime'])."Z</datetime>\n".
                                     "                  <text>".htmlspecialchars($answer['text'], ENT_XML1, "UTF-8")."</text>\n".
                                     "                </revision>\n";
                            }

                            echo "              </revisions>\n".
                                 "            </answer>\n";
                        }
                    }
                    else
                    {
                        echo "            <failed/>\n";
                    }

                    echo "          </answers>\n".
                         "        </question>\n";
                }
            }
        }
        else
        {
            echo "        <failed/>\n";
        }

        echo "      </questions>\n".
             "    </mission>\n";
    }
}

echo "  </missions>\n".
     "</gba-data>\n";



?>
