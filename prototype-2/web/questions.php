<?php
/* Copyright (C) 2012-2023 Stephan Kreutzer
 *
 * This file is part of GBA.
 *
 * GBA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * GBA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with GBA. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/questions.php
 * @author Stephan Kreutzer
 * @since 2019-07-22
 */



if (isset($_GET['id_mission']) !== true)
{
    http_response_code(400);
    exit(-1);
}

$idMission = (int)$_GET['id_mission'];

require_once("./libraries/database.inc.php");

if (Database::Get()->IsConnected() !== true)
{
    http_response_code(500);
    exit(-1);
}

$mission = Database::Get()->Query("SELECT `title`\n".
                                  "FROM `".Database::Get()->GetPrefix()."missions`\n".
                                  "WHERE `id`=?\n",
                                  array($idMission),
                                  array(Database::TYPE_INT));

if (is_array($mission) !== true)
{
    http_response_code(500);
    exit(-1);
}

if (count($mission) <= 0)
{
    http_response_code(404);
    exit(-1);
}

$mission = $mission[0];


echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n".
     "<!DOCTYPE html\n".
     "    PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"\n".
     "    \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n".
     "<html version=\"-//W3C//DTD XHTML 1.1//EN\" xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.w3.org/1999/xhtml http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd\" xml:lang=\"en\" lang=\"en\">\n".
     "  <head>\n".
     "    <meta http-equiv=\"content-type\" content=\"application/xhtml+xml; charset=UTF-8\"/>\n".
     "    <title>Questions</title>\n".
     "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>\n".
     "  </head>\n".
     "  <body>\n".
     "    <div>\n".
     "      <h2>Questions</h2>\n".
     "      <div>\n".
     "        Mission: ".$mission['title']."\n".
     "      </div>\n";

$errors = "";

if (isset($_POST['title']) === true)
{
    if (strlen($_POST['title']) <= 0)
    {
        $errors .= "<p>Error: Input field empty.</p>";
    }

    if (strlen($errors) <= 0)
    {
        if (Database::Get()->IsConnected() !== true)
        {
            $errors .= "<p>Error: Database isn't connected.</p>";
        }
    }

    if (strlen($errors) <= 0)
    {
        if (Database::Get()->BeginTransaction() !== true)
        {
            $errors .= "<p>Error: Can't begin database transaction.</p>";
        }
    }

    $id = -1;

    if (strlen($errors) <= 0)
    {
        $id = Database::Get()->Insert("INSERT INTO `".Database::Get()->GetPrefix()."questions` (`id`,\n".
                                      "    `title`,\n".
                                      "    `id_missions`)\n".
                                      "VALUES (?, ?, ?)\n",
                                      array(NULL, $_POST['title'], $idMission),
                                      array(Database::TYPE_NULL, Database::TYPE_STRING, Database::TYPE_INT));

        if ($id <= 0)
        {
            Database::Get()->RollbackTransaction();

            $errors .= "<p>Error: Insertion failed.</p>";
        }
    }

    if (strlen($errors) <= 0)
    {
        if (Database::Get()->CommitTransaction() !== true)
        {
            $errors .= "<p>Error: Committing the transaction failed.</p>";
        }
    }
}

if (strlen($errors) > 0)
{
    echo "      <div>\n".
         "        ".$errors."\n".
         "      </div>\n";

    $errors = "";
}

echo "      <div>\n".
     "        <form action=\"questions.php?id_mission=".$idMission."\" method=\"post\">\n".
     "          <fieldset>\n".
     "            <textarea name=\"title\" rows=\"1\" cols=\"80\"></textarea>\n".
     "            <input type=\"submit\" name=\"submit\" value=\"Add Question\"/>\n".
     "          </fieldset>\n".
     "        </form>\n".
     "      </div>\n";

$questions = array();

if (strlen($errors) <= 0)
{
    if (Database::Get()->IsConnected() !== true)
    {
        $errors .= "<p>Error: Database isn't connected.</p>";
    }

    if (strlen($errors) <= 0)
    {
        $questions = Database::Get()->Query("SELECT `id`,\n".
                                            "    `title`\n".
                                            "FROM `".Database::Get()->GetPrefix()."questions`\n".
                                            "WHERE `id_missions`=?\n".
                                            "ORDER BY `id` DESC",
                                            array($idMission),
                                            array(Database::TYPE_INT));

        if (is_array($questions) !== true)
        {
            $errors .= "<p>Error: Database query failed.</p>";
        }
    }
}

if (strlen($errors) <= 0)
{
    if (count($questions) > 0)
    {
        echo "      <ul>\n";

        foreach ($questions as $question)
        {
            echo "        <li><a href=\"answers.php?id_question=".((int)$question['id'])."\">".htmlspecialchars($question['title'], ENT_XHTML, "UTF-8")."</a></li>\n";
        }

        echo "      </ul>\n";
    }
}
else
{
    echo "      <div>\n".
         "        ".$errors."\n".
         "      </div>\n";

    $errors = "";
}

echo "      <div>\n".
     "        <a href=\"./missions.php\">Back to Missions</a>\n".
     "      </div>\n".
     "    </div>\n".
     "  </body>\n".
     "</html>\n".
     "\n";


?>
