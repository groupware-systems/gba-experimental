<?php
/* Copyright (C) 2012-2023 Stephan Kreutzer
 *
 * This file is part of GBA.
 *
 * GBA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * GBA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with GBA. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/questions_answers.php
 * @author Stephan Kreutzer
 * @since 2019-08-22
 */



if (isset($_GET['id_mission']) !== true)
{
    http_response_code(400);
    exit(-1);
}

$idMission = (int)$_GET['id_mission'];

require_once("./libraries/database.inc.php");

if (Database::Get()->IsConnected() !== true)
{
    http_response_code(500);
    exit(-1);
}

$mission = Database::Get()->Query("SELECT `title`\n".
                                  "FROM `".Database::Get()->GetPrefix()."missions`\n".
                                  "WHERE `id`=?\n",
                                  array($idMission),
                                  array(Database::TYPE_INT));

if (is_array($mission) !== true)
{
    http_response_code(500);
    exit(-1);
}

if (count($mission) <= 0)
{
    http_response_code(404);
    exit(-1);
}

$mission = $mission[0];


echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n".
     "<!DOCTYPE html\n".
     "    PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"\n".
     "    \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n".
     "<html version=\"-//W3C//DTD XHTML 1.1//EN\" xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.w3.org/1999/xhtml http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd\" xml:lang=\"en\" lang=\"en\">\n".
     "  <head>\n".
     "    <meta http-equiv=\"content-type\" content=\"application/xhtml+xml; charset=UTF-8\"/>\n".
     "    <title>Questions &amp; Answers</title>\n".
     "    <style type=\"text/css\">\n".
     "      .deleted\n".
     "      {\n".
     "          display: none;\n".
     "      }\n".
     "    </style>\n".
     /*"    <script type=\"text/javascript\" src=\"answers_controls.js\"></script>\n".*/
     "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>\n".
     "  </head>\n".
     "  <body>\n".
     "    <div>\n".
     "      <h2>Questions &amp; Answers</h2>\n".
     "      <div>\n".
     "        Mission: ".$mission['title']."\n".
     "      </div>\n";

$errors = "";

if (strlen($errors) > 0)
{
    echo "      <div>\n".
         "        ".$errors."\n".
         "      </div>\n";

    $errors = "";
}

$questions = array();

if (strlen($errors) <= 0)
{
    if (Database::Get()->IsConnected() !== true)
    {
        $errors .= "<p>Error: Database isn't connected.</p>";
    }

    if (strlen($errors) <= 0)
    {
        /**
         * @todo Without the need to render revisions, maybe the amount
         *     of queried rows could be reduced.
         */
        $questions = Database::Get()->Query("SELECT `".Database::Get()->GetPrefix()."questions`.`id` AS `questions_id`,\n".
                                            "    `".Database::Get()->GetPrefix()."questions`.`title` AS `questions_title`,\n".
                                            "    `".Database::Get()->GetPrefix()."answers`.`id` AS `answers_id`,\n".
                                            "    `".Database::Get()->GetPrefix()."answer_revisions`.`id` AS `answer_revisions_id`,\n".
                                            "    `".Database::Get()->GetPrefix()."answer_revisions`.`text` AS `answer_revisions_text`,\n".
                                            "    `".Database::Get()->GetPrefix()."answer_revisions`.`revision_datetime` AS `answer_revisions_revision_datetime`\n".
                                            "FROM `".Database::Get()->GetPrefix()."questions`\n".
                                            "INNER JOIN `".Database::Get()->GetPrefix()."answers` ON\n".
                                            "    `".Database::Get()->GetPrefix()."questions`.`id` =\n".
                                            "    `".Database::Get()->GetPrefix()."answers`.`id_questions`\n".
                                            "INNER JOIN `".Database::Get()->GetPrefix()."answer_revisions` ON\n".
                                            "    `".Database::Get()->GetPrefix()."answers`.`id` =\n".
                                            "    `".Database::Get()->GetPrefix()."answer_revisions`.`id_answers`\n".
                                            "WHERE `".Database::Get()->GetPrefix()."questions`.`id_missions`=?\n".
                                            "ORDER BY `".Database::Get()->GetPrefix()."questions`.`id` DESC,\n".
                                            "    `".Database::Get()->GetPrefix()."answers`.`id` DESC,\n".
                                            "    `".Database::Get()->GetPrefix()."answer_revisions`.`revision_datetime` DESC",
                                            array($idMission),
                                            array(Database::TYPE_INT));

        if (is_array($questions) !== true)
        {
            $errors .= "<p>Error: Database query failed.</p>";
        }

        if (is_array($questions) !== true)
        {
            $errors .= "<p>Error: Database query failed.</p>";
        }
    }
}

if (strlen($errors) <= 0)
{
    if (count($questions) > 0)
    {
        echo "      <ul>\n";

        $lastQuestionId = -1;
        $lastAnswerId = -1;

        foreach ($questions as $question)
        {
            if ($lastQuestionId != (int)$question['questions_id'])
            {
                if ($lastAnswerId >= 0)
                {
                    echo "    </tbody>\n".
                         "  </table>\n".
                         "</div>\n".
                         "            </li>\n".
                         "          </ul>\n";
                }

                if ($lastQuestionId >= 0)
                {
                    echo "        </li>\n";
                }

                $lastQuestionId = (int)$question['questions_id'];
                $lastAnswerId = -1;

                echo "        <li>\n".
                     "          ".htmlspecialchars($question['questions_title'], ENT_XHTML, "UTF-8")."  <a href=\"answers.php?id_question=".$question['questions_id']."\">Open</a>\n";
            }

            if ($lastAnswerId != (int)$question['answers_id'])
            {
                $deleted = "";

                if (strlen($question['answer_revisions_text']) <= 0)
                {
                    $deleted = " class=\"deleted\"";
                }

                if ($lastAnswerId >= 0)
                {
                    echo "    </tbody>\n".
                         "  </table>\n".
                         "</div>\n".
                         "            </li>\n".
                         "            <li".$deleted.">\n";
                }
                else
                {
                    echo "          <ul>\n".
                         "            <li".$deleted.">\n";
                }

                $lastAnswerId = (int)$question['answers_id'];

                /*echo "<span id=\"question_".$lastQuestionId."_answer_".$lastAnswerId."\">".htmlspecialchars($question['answer_revisions_text'], ENT_XHTML, "UTF-8")."</span> <a href=\"#\" onclick=\"LoadInputControl(".$question['questions_id'].", ".$lastAnswerId."); return false;\">Edit</a> <a href=\"#\" onclick=\"ToggleRevisions('revisions_answer_".$lastAnswerId."'); return false;\">Revisions</a>\n".*/
                echo "<span id=\"question_".$lastQuestionId."_answer_".$lastAnswerId."\">".htmlspecialchars($question['answer_revisions_text'], ENT_XHTML, "UTF-8")."</span>\n".
                     "<div id=\"question_".$lastQuestionId."_revisions_answer_".$lastAnswerId."\" style=\"display: none;\">\n".
                     "  <table border=\"1\">\n".
                     "    <thead>\n".
                     "      <tr>\n".
                     "        <th>Timestamp (UTC)</th>\n".
                     "        <th>Version</th>\n".
                     "      </tr>\n".
                     "    </thead>\n".
                     "    <tbody>\n";
            }

            echo "      <tr>\n".
                 "        <td>".str_replace(" ", "T", $question['answer_revisions_revision_datetime'])."Z</td>\n".
                 "        <td>".htmlspecialchars($question['answer_revisions_text'], ENT_XHTML, "UTF-8")."</td>\n".
                 "      </tr>\n";
        }

        if ($lastAnswerId >= 0)
        {
            echo "    </tbody>\n".
                 "  </table>\n".
                 "</div>\n".
                 "            </li>\n".
                 "          </ul>\n";
        }

        echo "        </li>\n".
             "      </ul>\n";
    }
}
else
{
    echo "      <div>\n".
         "        ".$errors."\n".
         "      </div>\n";

    $errors = "";
}

echo "      <div>\n".
     "        <a href=\"./missions.php\">Back to Missions</a>\n".
     "      </div>\n".
     "    </div>\n".
     "  </body>\n".
     "</html>\n".
     "\n";

?>
