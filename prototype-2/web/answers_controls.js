/* Copyright (C) 2019-2023 Stephan Kreutzer
 *
 * This file is part of GBA.
 *
 * GBA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * GBA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with GBA. If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

function LoadInputControl(questionId, editAnswerId)
{
    let inputControlDiv = document.getElementById('input_control');

    if (inputControlDiv == null)
    {
        return -1;
    }

    for (let i = 0; i < inputControlDiv.childNodes.length; i++)
    {
        removeNode(inputControlDiv.childNodes[i], inputControlDiv);
    }

    let form = document.createElement("form");
    form.setAttribute("method", "post");
    form.setAttribute("action", "answers.php?id_question=" + questionId);

    let fieldset = document.createElement("fieldset");

    let textarea = document.createElement("textarea");
    textarea.setAttribute("name", "text");
    textarea.setAttribute("rows", "24");
    textarea.setAttribute("cols", "80");

    if (editAnswerId > 0)
    {
        let answerTextSpan = document.getElementById("answer_" + editAnswerId);

        if (answerTextSpan != null)
        {
            let textareaText = document.createTextNode(answerTextSpan.innerText);
            textarea.appendChild(textareaText);
        }
    }

    let submit = document.createElement("input");
    submit.setAttribute("type", "submit");
    submit.setAttribute("name", "submit");

    if (editAnswerId > 0)
    {
        submit.setAttribute("value", "Update Answer");
    }
    else
    {
        submit.setAttribute("value", "Add new Answer");
    }

    let cancel = null;

    if (editAnswerId > 0)
    {
        cancel = document.createElement("input");
        cancel.setAttribute("type", "button");
        cancel.setAttribute("onclick", "LoadInputControl(" + questionId + ", 0);");
        cancel.setAttribute("value", "Cancel");
    }

    let editAnswerField = null;

    if (editAnswerId > 0)
    {
        editAnswerField = document.createElement("input");
        editAnswerField.setAttribute("type", "hidden");
        editAnswerField.setAttribute("name", "id_answer_edit");
        editAnswerField.setAttribute("value", editAnswerId);
    }

    fieldset.appendChild(textarea);
    fieldset.appendChild(submit);

    if (cancel != null)
    {
        fieldset.appendChild(cancel);
    }
    
    if (editAnswerField != null)
    {
        fieldset.appendChild(editAnswerField);
    }

    form.appendChild(fieldset);
    inputControlDiv.appendChild(form);

    return 0;
}

function ToggleRevisions(id)
{
    let revisionsDiv = document.getElementById(id);

    if (revisionsDiv == null)
    {
        return -1;
    }

    if (revisionsDiv.style.display === "block")
    {
        revisionsDiv.style.display = "none";
    }
    else
    {
        revisionsDiv.style.display = "block";
    }

    return 0;
}

// Stupid JavaScript has a cloneNode(deep), but no removeNode(deep).
function removeNode(element, parent)
{
    while (element.hasChildNodes() == true)
    {
        removeNode(element.lastChild, element);
    }

    parent.removeChild(element);
}
