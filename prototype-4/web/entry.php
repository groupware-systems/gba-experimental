<?php
/* Copyright (C) 2012-2023 Stephan Kreutzer
 *
 * This file is part of GBA.
 *
 * GBA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * GBA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with GBA. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/entry.php
 * @author Stephan Kreutzer
 * @since 2019-08-26
 */



$idEntry = null;
$idLevel = null;

if (isset($_GET['id']) === true)
{
    $idEntry = (int)$_GET['id'];
}

if (isset($_GET['level']) === true)
{
    $idLevel = (int)$_GET['level'];
}

require_once("./libraries/database.inc.php");

if (Database::Get()->IsConnected() !== true)
{
    http_response_code(500);
    exit(-1);
}

if (Database::Get()->BeginTransaction() !== true)
{
    http_response_code(500);
    exit(-1);
}

$levels = null;

if ($idLevel == null)
{
    if ($idEntry != null)
    {
        $entry = Database::Get()->Query("SELECT `id_levels`\n".
                                        "FROM `".Database::Get()->GetPrefix()."entries`\n".
                                        "WHERE `id`=?",
                                        array($idEntry),
                                        array(Database::TYPE_INT));

        if (is_array($entry) !== true)
        {
            Database::Get()->RollbackTransaction();
            http_response_code(500);
            exit(-1);
        }

        if (count($entry) <= 0)
        {
            Database::Get()->RollbackTransaction();
            http_response_code(404);
            exit(-1);
        }

        // Trigger querying table 'levels'.
        $idLevel = (int)$entry[0]['id_levels'];
    }
    else
    {
        $levels = Database::Get()->QueryUnsecure("SELECT `id`,\n".
                                                 "    `title`,\n".
                                                 "    `ordinal`\n".
                                                 "FROM `".Database::Get()->GetPrefix()."levels`\n".
                                                 "ORDER BY `ordinal` ASC");

        if (is_array($levels) !== true)
        {
            Database::Get()->RollbackTransaction();
            http_response_code(500);
            exit(-1);
        }

        if (count($levels) <= 0)
        {
            Database::Get()->RollbackTransaction();
            http_response_code(404);
            exit(-1);
        }

        // Avoid querying table 'levels' again.
    }
}

if ($idLevel != null)
{
    if ($idEntry != null)
    {
        $levels = Database::Get()->Query("SELECT `id`,\n".
                                         "    `title`,\n".
                                         "    `ordinal`\n".
                                         "FROM `".Database::Get()->GetPrefix()."levels`\n".
                                         "WHERE 1\n".
                                         "ORDER BY `id`=? DESC,\n".
                                         "    `ordinal` ASC",
                                         array($idLevel),
                                         array(Database::TYPE_INT));

        if (is_array($levels) !== true)
        {
            Database::Get()->RollbackTransaction();
            http_response_code(500);
            exit(-1);
        }

        if (count($levels) <= 0)
        {
            Database::Get()->RollbackTransaction();
            http_response_code(404);
            exit(-1);
        }

        if (((int)$levels[0]['id']) != $idLevel)
        {
            Database::Get()->RollbackTransaction();
            http_response_code(404);
            exit(-1);
        }
    }
    else
    {
        $levels = Database::Get()->QueryUnsecure("SELECT `id`,\n".
                                                 "    `title`,\n".
                                                 "    `ordinal`\n".
                                                 "FROM `".Database::Get()->GetPrefix()."levels`\n".
                                                 "ORDER BY `ordinal` ASC");

        if (is_array($levels) !== true)
        {
            Database::Get()->RollbackTransaction();
            http_response_code(500);
            exit(-1);
        }

        if (count($levels) <= 0)
        {
            Database::Get()->RollbackTransaction();
            http_response_code(404);
            exit(-1);
        }
    }
}

$idLevel = (int)$levels[0]['id'];

$idLevelParent = null;
$idLevelChildren = null;

for ($i = 1, $max = count($levels); $i < $max; $i++)
{
    if (((int)$levels[$i]['ordinal']) < ((int)$levels[0]['ordinal']))
    {
        // Overwrite.
        $idLevelParent = (int)$levels[$i]['ordinal'];
    }

    if (((int)$levels[$i]['ordinal']) > ((int)$levels[0]['ordinal']))
    {
        // Is available via query of table 'entries' too.
        $idLevelChildren = (int)$levels[$i]['ordinal'];
        break;
    }
}

if ($idEntry === null)
{
    $idLevelChildren = (int)$levels[0]['ordinal'];
}

$idEntryTarget = null;

if (isset($_POST['text']) === true &&
    isset($_POST['submit']) === true)
{
    if ($_POST['submit'] === "Add")
    {
        if ($idLevelChildren == null)
        {
            // There should be no form for adding a child entry anyway.
            Database::Get()->RollbackTransaction();
            http_response_code(500);
            exit(-1);
        }

        $idEntryNew = null;

        if ($idEntry !== null)
        {
            $idEntryNew = Database::Get()->Insert("INSERT INTO `".Database::Get()->GetPrefix()."entries` (`id`,\n".
                                                  "    `id_levels`,\n".
                                                  "    `id_entries`)\n".
                                                  "VALUES (?, ?, ?)",
                                                  array(NULL, $idLevelChildren, $idEntry),
                                                  array(Database::TYPE_NULL, Database::TYPE_INT, Database::TYPE_INT));
        }
        else
        {
            $idEntryNew = Database::Get()->Insert("INSERT INTO `".Database::Get()->GetPrefix()."entries` (`id`,\n".
                                                  "    `id_levels`,\n".
                                                  "    `id_entries`)\n".
                                                  "VALUES (?, ?, ?)",
                                                  array(NULL, $idLevelChildren, NULL),
                                                  array(Database::TYPE_NULL, Database::TYPE_INT, Database::TYPE_NULL));
        }

        if ($idEntryNew <= 0)
        {
            Database::Get()->RollbackTransaction();
            http_response_code(500);
            exit(-1);
        }

        $idEntryTarget = $idEntryNew;
    }
    else if ($_POST['submit'] === "Update")
    {
        if ($idEntry == null)
        {
            Database::Get()->RollbackTransaction();
            http_response_code(400);
            exit(-1);
        }

        $idEntryTarget = $idEntry;
    }
    else
    {
        Database::Get()->RollbackTransaction();
        http_response_code(400);
        exit(-1);
    }

    $idEntryRevision = Database::Get()->Insert("INSERT INTO `".Database::Get()->GetPrefix()."entry_revisions` (`id`,\n".
                                               "    `text`,\n".
                                               "    `revision_datetime`,\n".
                                               "    `id_entries`)\n".
                                               "VALUES (?, ?, UTC_TIMESTAMP(), ?)",
                                               array(NULL, $_POST['text'], $idEntryTarget),
                                               array(Database::TYPE_NULL, Database::TYPE_STRING, Database::TYPE_INT));

    if ($idEntryRevision <= 0)
    {
        Database::Get()->RollbackTransaction();
        http_response_code(500);
        exit(-1);
    }
}

$entries = null;

if ($idEntry !== null)
{
    $entries = Database::Get()->Query("SELECT `".Database::Get()->GetPrefix()."entries`.`id` AS `entries_id`,\n".
                                      "    `".Database::Get()->GetPrefix()."entries`.`id_levels` AS `entries_id_levels`,\n".
                                      "    `".Database::Get()->GetPrefix()."entries`.`id_entries` AS `entries_id_entries`,\n".
                                      "    `".Database::Get()->GetPrefix()."entry_revisions`.`id` AS `entry_revisions_id`,\n".
                                      "    `".Database::Get()->GetPrefix()."entry_revisions`.`text` AS `entry_revisions_text`,\n".
                                      "    `".Database::Get()->GetPrefix()."entry_revisions`.`revision_datetime` AS `entry_revisions_revision_datetime`\n".
                                      "FROM `".Database::Get()->GetPrefix()."entry_revisions`\n".
                                      "INNER JOIN `".Database::Get()->GetPrefix()."entries` ON\n".
                                      "    `".Database::Get()->GetPrefix()."entries`.`id` =\n".
                                      "    `".Database::Get()->GetPrefix()."entry_revisions`.`id_entries`\n".
                                      "WHERE `".Database::Get()->GetPrefix()."entries`.`id`=? OR\n".
                                      "    `".Database::Get()->GetPrefix()."entries`.`id_entries`=?\n".
                                      // This selects the current $idEntry (parent of child list) first!
                                      // Is used to pick it out from the other sub-entries, per hard-coded $entries[0].
                                      "ORDER BY `".Database::Get()->GetPrefix()."entries`.`id`=? DESC,\n".
                                      "    `".Database::Get()->GetPrefix()."entries`.`id` DESC,\n".
                                      "    `".Database::Get()->GetPrefix()."entry_revisions`.`revision_datetime` DESC",
                                      array($idEntry, $idEntry, $idEntry),
                                      array(Database::TYPE_INT, Database::TYPE_INT, Database::TYPE_INT));

    if (is_array($entries) !== true)
    {
        Database::Get()->RollbackTransaction();
        http_response_code(500);
        exit(-1);
    }

    if (count($entries) <= 0)
    {
        Database::Get()->RollbackTransaction();
        http_response_code(404);
        exit(-1);
    }

    if ($idEntryTarget != null)
    {
        if (((int)$entries[0]['entries_id']) != $idEntry)
        {
            Database::Get()->RollbackTransaction();
            http_response_code(404);
            exit(-1);
        }
    }
}
else
{
    $entries = Database::Get()->QueryUnsecure("SELECT `".Database::Get()->GetPrefix()."entries`.`id` AS `entries_id`,\n".
                                              "    `".Database::Get()->GetPrefix()."entries`.`id_levels` AS `entries_id_levels`,\n".
                                              "    `".Database::Get()->GetPrefix()."entries`.`id_entries` AS `entries_id_entries`,\n".
                                              "    `".Database::Get()->GetPrefix()."entry_revisions`.`id` AS `entry_revisions_id`,\n".
                                              "    `".Database::Get()->GetPrefix()."entry_revisions`.`text` AS `entry_revisions_text`,\n".
                                              "    `".Database::Get()->GetPrefix()."entry_revisions`.`revision_datetime` AS `entry_revisions_revision_datetime`\n".
                                              "FROM `".Database::Get()->GetPrefix()."entry_revisions`\n".
                                              "INNER JOIN `".Database::Get()->GetPrefix()."entries` ON\n".
                                              "    `".Database::Get()->GetPrefix()."entries`.`id` =\n".
                                              "    `".Database::Get()->GetPrefix()."entry_revisions`.`id_entries`\n".
                                              "WHERE `".Database::Get()->GetPrefix()."entries`.`id_entries` IS NULL\n".
                                              "ORDER BY `".Database::Get()->GetPrefix()."entries`.`id` DESC,\n".
                                              "    `".Database::Get()->GetPrefix()."entry_revisions`.`revision_datetime` DESC");

    if (is_array($entries) !== true)
    {
        Database::Get()->RollbackTransaction();
        http_response_code(500);
        exit(-1);
    }
}

if (count($entries) > 0 &&
    $idEntry !== null &&
    $idLevel !== null)
{
    if (((int)$entries[0]['entries_id_levels']) != $idLevel)
    {
        Database::Get()->RollbackTransaction();
        http_response_code(400);
        exit(-1);
    }
}

if (Database::Get()->CommitTransaction() !== true)
{
    Database::Get()->RollbackTransaction();
    http_response_code(500);
    exit(-1);
}


echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n".
     "<!DOCTYPE html\n".
     "    PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"\n".
     "    \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n".
     "<html version=\"-//W3C//DTD XHTML 1.1//EN\" xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.w3.org/1999/xhtml http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd\" xml:lang=\"en\" lang=\"en\">\n".
     "  <head>\n".
     "    <meta http-equiv=\"content-type\" content=\"application/xhtml+xml; charset=UTF-8\"/>\n".
     "    <title>".htmlspecialchars($levels[0]['title'], ENT_XHTML, "UTF-8")."</title>\n".
     "    <style type=\"text/css\">\n".
     "      .deleted\n".
     "      {\n".
     "          display: none;\n".
     "      }\n".
     "    </style>\n".
     "    <script type=\"text/javascript\" src=\"entry_controls.js\"></script>\n".
     "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>\n".
     "  </head>\n".
     "  <body>\n".
     "    <div>\n".
     "      <h2>".htmlspecialchars($levels[0]['title'], ENT_XHTML, "UTF-8")."</h2>\n";

if ($idEntry !== null)
{
    if (is_numeric($entries[0]['entries_id_entries']) === true)
    {
        $levelHintString = "";

        if ($idLevelParent != null)
        {
            $levelHintString .= "&amp;level=".$idLevelParent;
        }

        echo "      <div>\n".
             "        <a href=\"entry.php?id=".((int)$entries[0]['entries_id_entries']).$levelHintString."\">Up</a>\n".
             "      </div>\n";
    }
    else
    {
        echo "      <div>\n".
             "        <a href=\"entry.php\">Up</a>\n".
             "      </div>\n";
    }
}
else
{
    echo "      <div>\n".
         "        <a href=\"index.php\">Main</a>\n".
         "      </div>\n";
}

if ($idEntry !== null)
{
    $lastEntryId = -1;

    foreach ($entries as $entry)
    {
        if ($lastEntryId != (int)$entry['entries_id'])
        {
            if ($lastEntryId >= 0)
            {
                break;
            }

            $lastEntryId = (int)$entry['entries_id'];

            $deleted = "";

            if (strlen($entry['entry_revisions_text']) <= 0)
            {
                $deleted = " class=\"deleted\"";
            }

            echo "        <div id=\"update_control\"></div>\n".
                 "        <span".$deleted." id=\"entry_text\">".htmlspecialchars($entry['entry_revisions_text'], ENT_XHTML, "UTF-8")."</span> <a href=\"#\" onclick=\"LoadInputControl(".$idEntry.", ".$idLevel."); return false;\">Edit</a> <a href=\"#\" onclick=\"ToggleRevisions(); return false;\">Revisions</a>\n".
                 "        <div id=\"revisions\" style=\"display: none;\">\n".
                 "          <table border=\"1\">\n".
                 "            <thead>\n".
                 "              <tr>\n".
                 "                <th>Timestamp (UTC)</th>\n".
                 "                <th>Version</th>\n".
                 "              </tr>\n".
                 "            </thead>\n".
                 "            <tbody>\n";
        }

        echo "              <tr>\n".
             "                <td>".str_replace(" ", "T", $entry['entry_revisions_revision_datetime'])."Z</td>\n".
             "                <td>".htmlspecialchars($entry['entry_revisions_text'], ENT_XHTML, "UTF-8")."</td>\n".
             "              </tr>\n";
    }

    echo "            </tbody>\n".
         "          </table>\n".
         "        </div>\n".
         "      </div>\n";
}

echo "      <ul>\n";

if (count($entries) > 0)
{
    $lastEntryId = -1;

    foreach ($entries as $entry)
    {
        if ($lastEntryId != (int)$entry['entries_id'])
        {
            $lastEntryId = (int)$entry['entries_id'];

            if ($idEntry !== null &&
                $lastEntryId == $idEntry)
            {
                continue;
            }

            $deleted = "";

            if (strlen($entry['entry_revisions_text']) <= 0)
            {
                $deleted = " class=\"deleted\"";
            }

            echo "        <li".$deleted.">\n".
                 "          <span>".htmlspecialchars($entry['entry_revisions_text'], ENT_XHTML, "UTF-8")."</span> <a href=\"entry.php?id=".$lastEntryId."&amp;level=".((int)$entry['entries_id_levels'])."\">View</a>\n".
                 "        </li>\n";
        }
    }
}

if ($idLevelChildren != null)
{
    $queryString = "";

    if ($idEntry !== null)
    {
        if (empty($queryString) != true)
        {
            $queryString .= "&amp;";
        }

        $queryString .= "id=".$idEntry;
    }

    if (empty($queryString) != true)
    {
        $queryString .= "&amp;";
    }

    $queryString .= "level=".((int)$levels[0]['id']);

    if (empty($queryString) != true)
    {
        $queryString = "?".$queryString;
    }

    echo "        <li>\n".
         "          <form action=\"entry.php".$queryString."\" method=\"post\">\n".
         "            <fieldset>\n".
         "              <textarea name=\"text\" rows=\"24\" cols=\"80\"></textarea>\n".
         "              <input type=\"submit\" name=\"submit\" value=\"Add\"/>\n".
         "            </fieldset>\n".
         "          </form>\n".
         "        </li>\n";
}

echo "      </ul>\n".
     "    </div>\n".
     "  </body>\n".
     "</html>\n".
     "\n";


?>
