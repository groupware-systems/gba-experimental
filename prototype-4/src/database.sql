-- Copyright (C) 2019  Stephan Kreutzer
--
-- This file is part of GBA.
--
-- GBA is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License version 3 or any later version,
-- as published by the Free Software Foundation.
--
-- GBA is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- GNU Affero General Public License 3 for more details.
--
-- You should have received a copy of the GNU Affero General Public License 3
-- along with GBA. If not, see <http://www.gnu.org/licenses/>.



CREATE DATABASE `gba` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE gba;

CREATE USER 'gbauser'@'localhost' IDENTIFIED BY 'password';
GRANT USAGE ON *.* TO 'gbauser'@'localhost' IDENTIFIED BY 'password' WITH MAX_QUERIES_PER_HOUR 0
  MAX_CONNECTIONS_PER_HOUR 0
  MAX_UPDATES_PER_HOUR 0
  MAX_USER_CONNECTIONS 0;
GRANT ALL PRIVILEGES ON `gba`.* TO 'gbauser'@'localhost';


CREATE TABLE `levels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text COLLATE utf8_bin NOT NULL,
  `ordinal` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE (`ordinal`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

INSERT INTO `levels` (`id`, `title`, `ordinal`) VALUES
(1, 'Mission', 1),
(2, 'Question', 2),
(3, 'Answer', 3);

CREATE TABLE `entries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_levels` int(11) NOT NULL,
  -- Parent
  `id_entries` int(11),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

CREATE TABLE `entry_revisions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` text COLLATE utf8_bin NOT NULL,
  `revision_datetime` DATETIME NOT NULL,
  `id_entries` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
