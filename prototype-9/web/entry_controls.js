/* Copyright (C) 2019-2023 Stephan Kreutzer
 *
 * This file is part of GBA.
 *
 * GBA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * GBA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with GBA. If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

function LoadEditControl(idEntry)
{
    let updateControlDiv = document.getElementById('update_control');
    let updateControlLink = document.getElementById('update_control_link');
    let entryTextSpan = document.getElementById("entry_text");

    if (updateControlDiv == null ||
        entryTextSpan == null)
    {
        return -1;
    }

    for (let i = updateControlDiv.childNodes.length - 1; i >= 0; i--)
    {
        removeNode(updateControlDiv.childNodes[i], updateControlDiv);
    }

    if (idEntry > 0)
    {
        {
            let form = document.createElement("form");
            form.setAttribute("method", "post");
            form.setAttribute("action", "entry.php?id=" + idEntry);

            let fieldset = document.createElement("fieldset");

            let textarea = document.createElement("textarea");
            textarea.setAttribute("name", "text");
            textarea.setAttribute("rows", "1");
            textarea.setAttribute("cols", "80");

            {
                let textareaText = document.createTextNode(entryTextSpan.innerText);
                textarea.appendChild(textareaText);
            }

            let submit = document.createElement("input");
            submit.setAttribute("type", "submit");
            submit.setAttribute("name", "submit");
            submit.setAttribute("value", "Update");

            let cancel = document.createElement("input");
            cancel.setAttribute("type", "button");
            cancel.setAttribute("onclick", "LoadEditControl(0);");
            cancel.setAttribute("value", "Cancel");

            fieldset.appendChild(textarea);
            fieldset.appendChild(submit);
            fieldset.appendChild(cancel);

            form.appendChild(fieldset);
            updateControlDiv.appendChild(form);
        }

        {
            let form = document.createElement("form");
            form.setAttribute("method", "post");
            form.setAttribute("action", "entry.php?id=" + idEntry);

            let fieldset = document.createElement("fieldset");

            let label = document.createElement("label");
            label.setAttribute("for", "parent-id");
            label.appendChild(document.createTextNode("Parent-ID"));

            let textarea = document.createElement("textarea");
            textarea.setAttribute("id", "parent-id");
            textarea.setAttribute("name", "parent-id");
            textarea.setAttribute("rows", "1");
            textarea.setAttribute("cols", "8");

            let submit = document.createElement("input");
            submit.setAttribute("type", "submit");
            submit.setAttribute("name", "submit");
            submit.setAttribute("value", "Update");

            let cancel = document.createElement("input");
            cancel.setAttribute("type", "button");
            cancel.setAttribute("onclick", "LoadEditControl(0);");
            cancel.setAttribute("value", "Cancel");

            fieldset.appendChild(label);
            fieldset.appendChild(textarea);
            fieldset.appendChild(submit);
            fieldset.appendChild(cancel);

            form.appendChild(fieldset);
            updateControlDiv.appendChild(form);
        }

        {
            let copyright = document.createElement("p");
            copyright.appendChild(document.createTextNode("By submitting a form, you confirm that you’re the author of the text/setting, or possess compatible permission to license and publish your input under the "));

            {
                let link = document.createElement("a");
                link.setAttribute("href", "https://creativecommons.org/licenses/by-sa/4.0/legalcode");
                link.appendChild(document.createTextNode("Creative Commons Attribution-ShareAlike 4.0 International"));
                copyright.appendChild(link);
            }

            copyright.appendChild(document.createTextNode("."));
            updateControlDiv.appendChild(copyright);
        }
    }

    if (updateControlLink != null)
    {
        if (idEntry > 0)
        {
            updateControlLink.setAttribute("style", "display: none;");
        }
        else
        {
            updateControlLink.setAttribute("style", "display: inline;");
        }
    }

    return 0;
}

function LoadAddControl(idEntry)
{
    let addControlDiv = document.getElementById('add_control');

    if (addControlDiv == null)
    {
        return -1;
    }

    for (let i = addControlDiv.childNodes.length - 1; i >= 0; i--)
    {
        removeNode(addControlDiv.childNodes[i], addControlDiv);
    }

    {
        let queryString = "";

        if (idEntry > 0)
        {
            queryString += "?id=" + idEntry;
        }

        let form = document.createElement("form");
        form.setAttribute("method", "post");
        form.setAttribute("action", "entry.php" + queryString);

        let fieldset = document.createElement("fieldset");

        let textarea = document.createElement("textarea");
        textarea.setAttribute("name", "text");
        textarea.setAttribute("rows", "1");
        textarea.setAttribute("cols", "80");

        let submit = document.createElement("input");
        submit.setAttribute("type", "submit");
        submit.setAttribute("name", "submit");
        submit.setAttribute("value", "Add");

        let cancel = document.createElement("input");
        cancel.setAttribute("type", "button");
        cancel.setAttribute("onclick", "UnloadAddControl(" + (idEntry > 0 ? "" + idEntry : "") + ");");
        cancel.setAttribute("value", "Cancel");

        let copyright = document.createElement("p");
        copyright.appendChild(document.createTextNode("By submitting the form, you confirm that you’re the author of the text or possess compatible permission to license and publish your input under the "));

        {
            let link = document.createElement("a");
            link.setAttribute("href", "https://creativecommons.org/licenses/by-sa/4.0/legalcode");
            link.appendChild(document.createTextNode("Creative Commons Attribution-ShareAlike 4.0 International"));
            copyright.appendChild(link);
        }

        copyright.appendChild(document.createTextNode("."));

        fieldset.appendChild(textarea);
        fieldset.appendChild(submit);
        fieldset.appendChild(cancel);
        fieldset.appendChild(copyright);

        form.appendChild(fieldset);
        addControlDiv.appendChild(form);
    }

    return 0;
}

function UnloadAddControl(idEntry)
{
    let addControlDiv = document.getElementById('add_control');

    if (addControlDiv == null)
    {
        return -1;
    }

    for (let i = addControlDiv.childNodes.length - 1; i >= 0; i--)
    {
        removeNode(addControlDiv.childNodes[i], addControlDiv);
    }

    if (idEntry <= 0)
    {
        idEntry = "";
    }

    let link = document.createElement("a");
    link.setAttribute("href", "#");
    link.setAttribute("onclick", "LoadAddControl(" + idEntry + "); return false;");
    link.appendChild(document.createTextNode("Add"));
    addControlDiv.appendChild(link);

    return 0;
}

function ToggleRevisions()
{
    let revisionsDiv = document.getElementById("revisions");

    if (revisionsDiv == null)
    {
        return -1;
    }

    if (revisionsDiv.style.display === "block")
    {
        revisionsDiv.style.display = "none";
    }
    else
    {
        revisionsDiv.style.display = "block";
    }

    return 0;
}

// Stupid JavaScript has a cloneNode(deep), but no removeNode(deep).
function removeNode(element, parent)
{
    // TODO: Reverse delete for performance.
    while (element.hasChildNodes() == true)
    {
        removeNode(element.lastChild, element);
    }

    parent.removeChild(element);
}
