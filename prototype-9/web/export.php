<?php
/* Copyright (C) 2012-2023 Stephan Kreutzer
 *
 * This file is part of GBA.
 *
 * GBA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * GBA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with GBA. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/export.php
 * @author Stephan Kreutzer
 * @since 2023-01-14
 */


require_once("./libraries/https.inc.php");
require_once("./license.inc.php");

if (isset($_GET["format"]) !== true)
{
    http_response_code(400);
    exit(0);
}

if ($_GET["format"] != "json")
{
    http_response_code(400);
    exit(0);
}

require_once("./libraries/database.inc.php");

if (Database::Get()->IsConnected() !== true)
{
    http_response_code(500);
    exit(-1);
}

$entries = Database::Get()->QueryUnsecure("SELECT `".Database::Get()->GetPrefix()."entries`.`id` AS `entries_id`,\n".
                                          "    `".Database::Get()->GetPrefix()."entries`.`id_entries` AS `entries_id_entries`,\n".
                                          "    `".Database::Get()->GetPrefix()."entries`.`ordinal` AS `entries_ordinal`,\n".
                                          "    `".Database::Get()->GetPrefix()."entry_revisions`.`id` AS `entry_revisions_id`,\n".
                                          "    `".Database::Get()->GetPrefix()."entry_revisions`.`text` AS `entry_revisions_text`,\n".
                                          "    `".Database::Get()->GetPrefix()."entry_revisions`.`revision_datetime` AS `entry_revisions_revision_datetime`,\n".
                                          "    `".Database::Get()->GetPrefix()."entry_revisions`.`id_users` AS `entry_revisions_id_users`,\n".
                                          "    `".Database::Get()->GetPrefix()."users`.`name` AS `users_name`\n".
                                          "FROM `".Database::Get()->GetPrefix()."entry_revisions`\n".
                                          "INNER JOIN `".Database::Get()->GetPrefix()."entries` ON\n".
                                          "    `".Database::Get()->GetPrefix()."entries`.`id` =\n".
                                          "    `".Database::Get()->GetPrefix()."entry_revisions`.`id_entries`\n".
                                          "INNER JOIN `".Database::Get()->GetPrefix()."users` ON\n".
                                          "    `".Database::Get()->GetPrefix()."users`.`id` =\n".
                                          "    `".Database::Get()->GetPrefix()."entry_revisions`.`id_users`\n".
                                          "WHERE 1\n".
                                          "ORDER BY `".Database::Get()->GetPrefix()."entries`.`ordinal` ASC,\n".
                                          "    `".Database::Get()->GetPrefix()."entries`.`id` DESC,\n".
                                          "    `".Database::Get()->GetPrefix()."entry_revisions`.`revision_datetime` DESC");

if (is_array($entries) !== true)
{
    http_response_code(500);
    exit(-1);
}

$tree = array();

foreach ($entries as $entry)
{
    if (array_key_exists((int)$entry["entries_id_entries"], $tree) != true)
    {
        $tree[(int)$entry["entries_id_entries"]] = array();
    }

    $tree[(int)$entry["entries_id_entries"]][] = $entry;
}

header("Content-Type: application/json");

echo "{\"copyright_license\":".json_encode(getContentLicenseUrl()).",".
     "\"entries\":[";

if (count($tree) <= 0)
{

}

if (count($tree[0]) <= 0)
{

}

output($tree, 0);

echo "]}";



/** @todo No consideration here for passing $tree per reference! */
function output($tree, $id)
{
    $lastEntryId = -1;
    $authors = array();
    $deleted = false;
    $entryOutputCount = 0;

    for ($i = 0, $max = count($tree[$id]); $i < $max; $i++)
    {
        $entry = $tree[$id][$i];

        if ($lastEntryId == (int)$entry["entries_id"])
        {
            if ($deleted == false)
            {
                if (in_array($entry["users_name"], $authors) != true)
                {
                    $authors[] = $entry["users_name"];
                }
            }

            continue;
        }
        else
        {
            $lastEntryId = (int)$entry["entries_id"];

            if (count($authors) > 0 &&
                $deleted == false)
            {
                echo ",\"authors\":[";

                for ($j = count($authors) - 1; $j >= 0; $j--)
                {
                    echo json_encode($authors[$j]);

                    if ($j > 0)
                    {
                        echo ",";
                    }
                }

                echo "]";

                $authors = array();
            }

            if (strlen($entry["entry_revisions_text"]) > 0)
            {
                $deleted = false;

                if ($entryOutputCount > 0)
                {
                    echo "},";
                }
            }
            else
            {
                $deleted = true;

                for (; $i < $max; $i++)
                {
                    $entry = $tree[$id][$i];

                    if ($lastEntryId == (int)$entry["entries_id"])
                    {
                        $i -= 1;
                        break;
                    }
                }

                continue;
            }

            $authors[] = $entry["users_name"];
        }

        $entryOutputCount += 1;

        echo "{\"text\":".json_encode($entry["entry_revisions_text"]).",".
             "\"last_revision_datetime\":".json_encode(str_replace(" ", "T", $entry["entry_revisions_revision_datetime"])."Z");

        if (array_key_exists((int)$entry["entries_id"], $tree) == true)
        {
            echo ",\"entries\":[";
            output($tree, (int)$entry["entries_id"]);
            echo "]";
        }
    }

    if (count($authors) > 0 &&
        $deleted == false)
    {
        echo ",\"authors\":[";

        for ($j = count($authors) - 1; $j >= 0; $j--)
        {
            echo json_encode($authors[$j]);

            if ($j > 0)
            {
                echo ",";
            }
        }

        echo "]";
    }

    if ($entryOutputCount > 0)
    {
        echo "}";
    }
}


?>
