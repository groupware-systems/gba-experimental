<?php
/* Copyright (C) 2012-2023 Stephan Kreutzer
 *
 * This file is part of GBA.
 *
 * GBA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * GBA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with GBA. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/entry.php
 * @author Stephan Kreutzer
 * @since 2019-08-26
 */



require_once("./libraries/https.inc.php");
require_once("./libraries/session.inc.php");
require_once("./libraries/user_defines.inc.php");

$idEntry = null;

if (isset($_GET['id']) === true)
{
    $idEntry = (int)$_GET['id'];
}

require_once("./libraries/database.inc.php");

if (Database::Get()->IsConnected() !== true)
{
    http_response_code(500);
    exit(-1);
}

if (Database::Get()->BeginTransaction() !== true)
{
    http_response_code(500);
    exit(-1);
}

$idEntryTarget = null;

if (defined("SESSION_ACTIVE") === true)
{
    if (isset($_POST['text']) === true &&
        isset($_POST['submit']) === true)
    {
        if ($_POST['submit'] === "Add")
        {
            $idEntryNew = null;

            if ($idEntry !== null)
            {
                $idEntryNew = Database::Get()->Insert("INSERT INTO `".Database::Get()->GetPrefix()."entries` (`id`,\n".
                                                      "    `id_entries`,\n".
                                                      "    `ordinal`)\n".
                                                      "VALUES (?, ?, 0)",
                                                      array(NULL, $idEntry),
                                                      array(Database::TYPE_NULL, Database::TYPE_INT));
            }
            else
            {
                $idEntryNew = Database::Get()->InsertUnsecure("INSERT INTO `".Database::Get()->GetPrefix()."entries` (`id`,\n".
                                                              "    `id_entries`,\n".
                                                              "    `ordinal`)\n".
                                                              "VALUES (NULL, NULL, 0)");
            }

            if ($idEntryNew <= 0)
            {
                Database::Get()->RollbackTransaction();
                http_response_code(500);
                exit(-1);
            }

            $idEntryTarget = $idEntryNew;
        }
        else if ($_POST['submit'] === "Update")
        {
            if ($idEntry == null)
            {
                Database::Get()->RollbackTransaction();
                http_response_code(400);
                exit(0);
            }

            $idEntryTarget = $idEntry;
        }
        else
        {
            Database::Get()->RollbackTransaction();
            http_response_code(400);
            exit(0);
        }

        $idEntryRevision = Database::Get()->Insert("INSERT INTO `".Database::Get()->GetPrefix()."entry_revisions` (`id`,\n".
                                                   "    `text`,\n".
                                                   "    `revision_datetime`,\n".
                                                   "    `id_users`,\n".
                                                   "    `id_entries`)\n".
                                                   "VALUES (?, ?, UTC_TIMESTAMP(), ?, ?)",
                                                   array(NULL, $_POST['text'], $_SESSION['user_id'], $idEntryTarget),
                                                   array(Database::TYPE_NULL, Database::TYPE_STRING, Database::TYPE_INT, Database::TYPE_INT));

        if ($idEntryRevision <= 0)
        {
            Database::Get()->RollbackTransaction();
            http_response_code(500);
            exit(-1);
        }
    }
    else if (isset($_GET['move']) === true)
    {
        if (isset($_GET['child-id']) !== true)
        {
            Database::Get()->RollbackTransaction();
            http_response_code(400);
            exit(0);
        }

        $childId = (int)$_GET['child-id'];

        $entries = null;

        if ($idEntry != null)
        {
            $entries = Database::Get()->Query("SELECT `id`,\n".
                                              "    `ordinal`\n".
                                              "FROM `".Database::Get()->GetPrefix()."entries`\n".
                                              "WHERE `id_entries`=?\n".
                                              "ORDER BY `ordinal` ASC,\n".
                                              "    `id` DESC",
                                              array($idEntry),
                                              array(Database::TYPE_INT));
        }
        else
        {
            $entries = Database::Get()->QueryUnsecure("SELECT `id`,\n".
                                                      "    `ordinal`\n".
                                                      "FROM `".Database::Get()->GetPrefix()."entries`\n".
                                                      "WHERE `id_entries` IS NULL\n".
                                                      "ORDER BY `ordinal` ASC,\n".
                                                      "    `id` DESC");
        }

        if (is_array($entries) !== true)
        {
            Database::Get()->RollbackTransaction();
            http_response_code(500);
            exit(-1);
        }

        $max = count($entries);

        if ($max <= 0)
        {
            Database::Get()->RollbackTransaction();
            http_response_code(404);
            exit(0);
        }

        $order = array();
        $found = null;

        for ($i = 0; $i < $max; $i++)
        {
            $order[$i + 1] = (int)$entries[$i]['id'];

            if ($order[$i + 1] == $childId)
            {
                $found = $i + 1;
            }
        }

        if ($found === null)
        {
            Database::Get()->RollbackTransaction();
            http_response_code(404);
            exit(0);
        }

        $move = $_GET['move'];

        if ($move === "up")
        {
            if ($found > 1)
            {
                $temp = $order[$found - 1];
                $order[$found - 1] = $order[$found];
                $order[$found] = $temp;
            }
        }
        else if ($move === "down")
        {
            if ($found < $max)
            {
                $temp = $order[$found + 1];
                $order[$found + 1] = $order[$found];
                $order[$found] = $temp;
            }
        }
        else
        {
            Database::Get()->RollbackTransaction();
            http_response_code(400);
            exit(0);
        }

        foreach ($order as $ordinal => $id)
        {
            $result = Database::Get()->Execute("UPDATE `".Database::Get()->GetPrefix()."entries`\n".
                                               "SET `ordinal`=?\n".
                                               "WHERE `id`=?",
                                               array($ordinal, $id),
                                               array(Database::TYPE_INT, Database::TYPE_INT));

            if ($result !== true)
            {
                Database::Get()->RollbackTransaction();
                http_response_code(500);
                exit(-1);
            }
        }
    }
}

$entries = null;

if ($idEntry !== null)
{
    $entries = Database::Get()->Query("SELECT `".Database::Get()->GetPrefix()."entries`.`id` AS `entries_id`,\n".
                                      "    `".Database::Get()->GetPrefix()."entries`.`id_entries` AS `entries_id_entries`,\n".
                                      "    `".Database::Get()->GetPrefix()."entries`.`ordinal` AS `entries_ordinal`,\n".
                                      "    `".Database::Get()->GetPrefix()."entry_revisions`.`id` AS `entry_revisions_id`,\n".
                                      "    `".Database::Get()->GetPrefix()."entry_revisions`.`text` AS `entry_revisions_text`,\n".
                                      "    `".Database::Get()->GetPrefix()."entry_revisions`.`revision_datetime` AS `entry_revisions_revision_datetime`,\n".
                                      "    `".Database::Get()->GetPrefix()."entry_revisions`.`id_users` AS `entry_revisions_id_users`,\n".
                                      "    `".Database::Get()->GetPrefix()."users`.`name` AS `users_name`\n".
                                      "FROM `".Database::Get()->GetPrefix()."entry_revisions`\n".
                                      "INNER JOIN `".Database::Get()->GetPrefix()."entries` ON\n".
                                      "    `".Database::Get()->GetPrefix()."entries`.`id` =\n".
                                      "    `".Database::Get()->GetPrefix()."entry_revisions`.`id_entries`\n".
                                      "INNER JOIN `".Database::Get()->GetPrefix()."users` ON\n".
                                      "    `".Database::Get()->GetPrefix()."users`.`id` =\n".
                                      "    `".Database::Get()->GetPrefix()."entry_revisions`.`id_users`\n".
                                      "WHERE `".Database::Get()->GetPrefix()."entries`.`id`=? OR\n".
                                      "    `".Database::Get()->GetPrefix()."entries`.`id_entries`=?\n".
                                      // This selects the current $idEntry (parent of child list) first!
                                      // Is used to pick it out from the other sub-entries, per hard-coded $entries[0].
                                      "ORDER BY `".Database::Get()->GetPrefix()."entries`.`id`=? DESC,\n".
                                      "    `".Database::Get()->GetPrefix()."entries`.`ordinal` ASC,\n".
                                      "    `".Database::Get()->GetPrefix()."entries`.`id` DESC,\n".
                                      "    `".Database::Get()->GetPrefix()."entry_revisions`.`revision_datetime` DESC",
                                      array($idEntry, $idEntry, $idEntry),
                                      array(Database::TYPE_INT, Database::TYPE_INT, Database::TYPE_INT));

    if (is_array($entries) !== true)
    {
        Database::Get()->RollbackTransaction();
        http_response_code(500);
        exit(-1);
    }

    if (count($entries) <= 0)
    {
        Database::Get()->RollbackTransaction();
        http_response_code(404);
        exit(-1);
    }

    if ($idEntryTarget != null)
    {
        if (((int)$entries[0]['entries_id']) != $idEntry)
        {
            Database::Get()->RollbackTransaction();
            http_response_code(404);
            exit(-1);
        }
    }
}
else
{
    $entries = Database::Get()->QueryUnsecure("SELECT `".Database::Get()->GetPrefix()."entries`.`id` AS `entries_id`,\n".
                                              "    `".Database::Get()->GetPrefix()."entries`.`id_entries` AS `entries_id_entries`,\n".
                                              "    `".Database::Get()->GetPrefix()."entries`.`ordinal` AS `entries_ordinal`,\n".
                                              "    `".Database::Get()->GetPrefix()."entry_revisions`.`id` AS `entry_revisions_id`,\n".
                                              "    `".Database::Get()->GetPrefix()."entry_revisions`.`text` AS `entry_revisions_text`,\n".
                                              "    `".Database::Get()->GetPrefix()."entry_revisions`.`revision_datetime` AS `entry_revisions_revision_datetime`,\n".
                                              "    `".Database::Get()->GetPrefix()."entry_revisions`.`id_users` AS `entry_revisions_id_users`,\n".
                                              "    `".Database::Get()->GetPrefix()."users`.`name` AS `users_name`\n".
                                              "FROM `".Database::Get()->GetPrefix()."entry_revisions`\n".
                                              "INNER JOIN `".Database::Get()->GetPrefix()."entries` ON\n".
                                              "    `".Database::Get()->GetPrefix()."entries`.`id` =\n".
                                              "    `".Database::Get()->GetPrefix()."entry_revisions`.`id_entries`\n".
                                              "INNER JOIN `".Database::Get()->GetPrefix()."users` ON\n".
                                              "    `".Database::Get()->GetPrefix()."users`.`id` =\n".
                                              "    `".Database::Get()->GetPrefix()."entry_revisions`.`id_users`\n".
                                              "WHERE `".Database::Get()->GetPrefix()."entries`.`id_entries` IS NULL\n".
                                              "ORDER BY `".Database::Get()->GetPrefix()."entries`.`ordinal` ASC,\n".
                                              "    `".Database::Get()->GetPrefix()."entries`.`id` DESC,\n".
                                              "    `".Database::Get()->GetPrefix()."entry_revisions`.`revision_datetime` DESC");

    if (is_array($entries) !== true)
    {
        Database::Get()->RollbackTransaction();
        http_response_code(500);
        exit(-1);
    }
}

if (Database::Get()->CommitTransaction() !== true)
{
    Database::Get()->RollbackTransaction();
    http_response_code(500);
    exit(-1);
}


/** @todo Title + header! */
echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n".
     "<!DOCTYPE html\n".
     "    PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"\n".
     "    \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n".
     "<html version=\"-//W3C//DTD XHTML 1.1//EN\" xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.w3.org/1999/xhtml http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd\" xml:lang=\"en\" lang=\"en\">\n".
     "  <head>\n".
     "    <meta http-equiv=\"content-type\" content=\"application/xhtml+xml; charset=UTF-8\"/>\n".
     "    <title>Entry</title>\n".
     "    <style type=\"text/css\">\n".
     "      body\n".
     "      {\n".
     "          font-family: monospace;\n".
     "      }\n".
     "\n".
     "      .deleted\n".
     "      {\n".
     "          display: none;\n".
     "      }\n".
     "    </style>\n".
     "    <script type=\"text/javascript\" src=\"entry_controls.js\"></script>\n".
     "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>\n".
     "  </head>\n".
     "  <body>\n".
     "    <div>\n".
     "      <h2>Entry</h2>\n";

if ($idEntry !== null)
{
    if (is_numeric($entries[0]['entries_id_entries']) === true)
    {
        echo "      <div>\n".
             "        <a href=\"index.php\">Main</a> <a href=\"entry.php\">Top</a> <a href=\"entry.php?id=".((int)$entries[0]['entries_id_entries'])."\">Up</a>\n".
             "      </div>\n";
    }
    else
    {
        echo "      <div>\n".
             "        <a href=\"index.php\">Main</a> <a href=\"entry.php\">Top</a>\n".
             "      </div>\n";
    }
}
else
{
    echo "      <div>\n".
         "        <a href=\"index.php\">Main</a>\n".
         "      </div>\n";
}

if ($idEntry !== null)
{
    $lastEntryId = -1;

    foreach ($entries as $entry)
    {
        if ($lastEntryId != (int)$entry['entries_id'])
        {
            if ($lastEntryId >= 0)
            {
                break;
            }

            $lastEntryId = (int)$entry['entries_id'];

            $deleted = "";

            if (strlen($entry['entry_revisions_text']) <= 0)
            {
                $deleted = " class=\"deleted\"";
            }

            echo "      <div id=\"update_control\"></div>\n".
                 "      <span".$deleted." id=\"entry_text\">".htmlspecialchars($entry['entry_revisions_text'], ENT_XHTML, "UTF-8")."</span>";

            if (defined("SESSION_ACTIVE") === true)
            {
                echo " <a href=\"#\" onclick=\"LoadInputControl(".$idEntry."); return false;\">Edit</a>";
            }

            echo " <a href=\"#\" onclick=\"ToggleRevisions(); return false;\">Revisions</a>\n".
                 "      <div id=\"revisions\" style=\"display: none;\">\n".
                 "        <table border=\"1\">\n".
                 "          <thead>\n".
                 "            <tr>\n".
                 "              <th>Timestamp (UTC)</th>\n".
                 "              <th>Author</th>\n".
                 "              <th>Version</th>\n".
                 "            </tr>\n".
                 "          </thead>\n".
                 "          <tbody>\n";
        }

        echo "            <tr>\n".
             "              <td>".str_replace(" ", "T", $entry['entry_revisions_revision_datetime'])."Z</td>\n".
             "              <td>".htmlspecialchars($entry['users_name'], ENT_XHTML, "UTF-8")."</td>\n".
             "              <td>".htmlspecialchars($entry['entry_revisions_text'], ENT_XHTML, "UTF-8")."</td>\n".
             "            </tr>\n";
    }

    echo "          </tbody>\n".
         "        </table>\n".
         "      </div>\n";
}

echo "      <hr/>\n";

$subentryHeaderPrinted = false;

if (defined("SESSION_ACTIVE") === true)
{
    if ($subentryHeaderPrinted != true)
    {
        echo "      <div>\n".
             "        <h3>Sub-Entries</h3>\n";

        $subentryHeaderPrinted = true;
    }

    $queryString = "";

    if ($idEntry !== null)
    {
        $queryString = "?id=".$idEntry;
    }

    echo "        <div>\n".
         "          <form action=\"entry.php".$queryString."\" method=\"post\">\n".
         "            <fieldset>\n".
         "              <textarea name=\"text\" rows=\"1\" cols=\"80\"></textarea>\n".
         "              <input type=\"submit\" name=\"submit\" value=\"Add\"/>\n".
         "            </fieldset>\n".
         "          </form>\n".
         "        </div>\n";
}

if (count($entries) > 0)
{
    $lastEntryId = -1;
    $subEntries = array();

    foreach ($entries as $entry)
    {
        /*
        if (defined("SESSION_ACTIVE") === true)
        {
            if ((int)$entry['entry_revisions_id_users'] !== (int)$_SESSION['user_id'] &&
                (int)$_SESSION['user_role'] !== USER_ROLE_ADMIN)
            {
                continue;
            }
        }
        */

        if ($lastEntryId != (int)$entry['entries_id'])
        {
            $lastEntryId = (int)$entry['entries_id'];

            if ($idEntry !== null &&
                $lastEntryId == $idEntry)
            {
                continue;
            }

            $subEntries[] = $entry;
        }
    }

    $max = count($subEntries);

    if ($max > 0)
    {
        if ($subentryHeaderPrinted != true)
        {
            echo "      <div>\n".
                 "        <h3>Sub-Entries</h3>\n";

            $subentryHeaderPrinted = true;
        }

        echo "        <ul>\n";

        for ($i = 0, $max = count($subEntries); $i < $max; $i++)
        {
            $currentEntryId = (int)$subEntries[$i]['entries_id'];

            $deleted = "";

            if (strlen($subEntries[$i]['entry_revisions_text']) <= 0)
            {
                $deleted = " class=\"deleted\"";
            }

            echo "          <li".$deleted.">\n".
                 "            ";

            if (defined("SESSION_ACTIVE") === true)
            {
                $parentIdString = "";

                if ($idEntry !== null)
                {
                    $parentIdString = "id=".$idEntry."&amp;";
                }

                // Yes, it's a bit problematic to have a link/HTTP-GET to trigger this operation,
                // as a crawler might hit it, but OK assumption/hope here is, a crawler isn't logged
                // in and therefore should not gain access to editing controls.

                if ($i == 0)
                {
                    echo "&#x2191; ";
                }
                else
                {
                    echo "<a href=\"?".$parentIdString."child-id=".$currentEntryId."&amp;move=up\">&#x2191;</a> ";
                }

                if ($i == ($max - 1))
                {
                    echo "&#x2193; ";
                }
                else
                {
                    echo "<a href=\"?".$parentIdString."child-id=".$currentEntryId."&amp;move=down\">&#x2193;</a> ";
                }
            }

            echo "<span>".htmlspecialchars($subEntries[$i]['entry_revisions_text'], ENT_XHTML, "UTF-8")."</span> <a href=\"entry.php?id=".$currentEntryId."\">View</a>\n".
                 "          </li>\n";
        }

        echo "        </ul>\n";
    }
}

if ($subentryHeaderPrinted == true)
{
    echo "      </div>\n";
}

echo "    </div>\n".
     "  </body>\n".
     "</html>\n".
     "\n";


?>
