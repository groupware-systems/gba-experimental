<?php
/* Copyright (C) 2017-2022 Stephan Kreutzer
 *
 * This file is part of GBA.
 *
 * GBA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * GBA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with GBA. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/libraries/session.inc.php
 * @author Stephan Kreutzer
 * @since 2017-04-23
 */



if (isset($_COOKIE) === true)
{
    if (isset($_COOKIE["PHPSESSID"]) === true)
    {
        if (isset($_SESSION) === true)
        {
            if (empty($_SESSION) == true)
            {
                if (@session_start() !== true)
                {
                    http_response_code(500);
                    exit(-1);
                }
            }
        }
        else
        {
            if (@session_start() !== true)
            {
                http_response_code(500);
                exit(-1);
            }
        }

        if (isset($_SESSION) === true)
        {
            if (empty($_SESSION) !== true)
            {
                if (isset($_SESSION['user_id']) === true &&
                    isset($_SESSION['user_role']) === true &&
                    isset($_SESSION['instance_path']) === true)
                {
                    $lhs = str_replace("\\", "/", dirname(__FILE__));
                    $rhs = str_replace("\\", "/", $_SESSION['instance_path'])."/libraries";

                    if ($lhs === $rhs)
                    {
                        define("SESSION_ACTIVE", true);
                    }
                    else
                    {
                        http_response_code(403);
                        exit(-1);
                    }
                }
            }
        }
        else
        {
            http_response_code(500);
            exit(-1);
        }
    }
}

if (defined("SESSION_ACTIVE") != true)
{
    if (isset($_SESSION) == true)
    {
        $_SESSION = array();
    }

    if (isset($_COOKIE) == true)
    {
        if (isset($_COOKIE[session_name()]) == true)
        {
            setcookie(session_name(), '', time()-42000, '/');
        }
    }
}



?>
