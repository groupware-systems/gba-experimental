<?php
/* Copyright (C) 2019  Stephan Kreutzer
 *
 * This file is part of GBA.
 *
 * GBA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * GBA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with GBA. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/index.php
 * @author Stephan Kreutzer
 * @since 2019-07-28
 */



require_once("./libraries/https.inc.php");

if (empty($_SESSION) === true)
{
    @session_start();
}

if (isset($_POST['logout']) === true &&
    isset($_SESSION['user_id']) === true)
{
    $_SESSION = array();

    if (isset($_COOKIE[session_name()]) == true)
    {
        setcookie(session_name(), '', time()-42000, '/');
    }
}

echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n".
     "<!DOCTYPE html\n".
     "    PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"\n".
     "    \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n".
     "<html version=\"-//W3C//DTD XHTML 1.1//EN\" xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.w3.org/1999/xhtml http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd\" xml:lang=\"en\" lang=\"en\">\n".
     "  <head>\n".
     "    <meta http-equiv=\"content-type\" content=\"application/xhtml+xml; charset=UTF-8\"/>\n".
     "    <title>GBA</title>\n".
     "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>\n".
     "    <style type=\"text/css\">\n".
     "      body\n".
     "      {\n".
     "          font-family: monospace;\n".
     "      }\n".
     "    </style>\n".
     "  </head>\n".
     "  <body>\n";

if (isset($_POST['name']) !== true ||
    isset($_POST['password']) !== true)
{
    echo "    <div>\n".
         "      <h1>GBA</h1>\n".
         "      <div>\n";

    if (isset($_POST['install_done']) == true)
    {
        if (@unlink(dirname(__FILE__)."/install/install.php") === true)
        {
            clearstatcache();
        }
        else
        {
            echo "        <p class=\"error\">\n".
                 "          The installation was already completed successfully, but it was unable to delete itself. Please delete at least the file <tt>\$/install/install.php</tt> or additionally the entire directory <tt>\$/install/</tt> manually.\n".
                 "        </p>\n";
        }
    }

    if (file_exists("./install/install.php") === true &&
        isset($_GET['skipinstall']) != true)
    {
        echo "        <form action=\"install/install.php\" method=\"post\">\n".
             "          <fieldset>\n".
             "            <input type=\"submit\" value=\"Install\"/><br/>\n".
             "          </fieldset>\n".
             "        </form>\n";

        require_once("./license.inc.php");
        echo getHTMLLicenseNotification("license");
    }
    else
    {
        require_once("./libraries/user_management.inc.php");

        if (isset($_SESSION['user_id']) === true &&
            isset($_SESSION['instance_path']) === true)
        {
            $lhs = str_replace("\\", "/", dirname(__FILE__));

            if ($lhs === $_SESSION['instance_path'])
            {
                echo "        <ul>\n".
                     "          <li><a href=\"entry.php\">Entries</a></li>\n".
                     "        </ul>\n";
            }

            echo "        <form action=\"index.php\" method=\"post\">\n".
                 "          <fieldset>\n".
                 "            <input type=\"submit\" name=\"logout\" value=\"Log out\"/><br/>\n".
                 "          </fieldset>\n".
                 "        </form>\n";
        }
        else
        {
            echo "        <p>\n".
                 "          Welcome!\n".
                 "        </p>\n".
                 "        <p>\n".
                 "          Login (or <a href=\"register.php\">register</a>):\n".
                 "        </p>\n".
                 "        <form action=\"index.php\" method=\"post\">\n".
                 "          <fieldset>\n".
                 "            <input name=\"name\" type=\"text\" size=\"20\" maxlength=\"60\"/> Name<br />\n".
                 "            <input name=\"password\" type=\"password\" size=\"20\" maxlength=\"60\"/> Password<br />\n".
                 "            <input type=\"submit\" value=\"OK\"/><br/>\n".
                 "          </fieldset>\n".
                 "        </form>\n";

            require_once("./license.inc.php");
            echo getHTMLLicenseNotification("license");
        }
    }

    echo "      </div>\n".
         "      <div>\n".
         "        <a href=\"license.php\">Licensing</a>\n".
         "      </div>\n".
         "    </div>\n".
         "  </body>\n".
         "</html>\n".
         "\n";
}
else
{
    require_once("./libraries/user_management.inc.php");

    $user = NULL;

    $result = getUserByName($_POST['name']);

    if (is_array($result) !== true)
    {
        echo "    <div>\n".
             "      <p class=\"error\">\n".
             "        Can’t connect to database.\n".
             "      </p>\n".
             "    </div>\n".
             "    <div>\n".
             "      <a href=\"license.php\">Licensing</a>\n".
             "    </div>\n".
             "  </body>\n".
             "</html>\n";

        exit(-1);
    }

    if (count($result) === 0)
    {
        echo "    <div>\n".
             "      <p class=\"error\">\n".
             "        Login failed.\n".
             "      </p>\n".
             "      <a href=\"index.php\">Retry</a>\n".
             "    </div>\n".
             "  </body>\n".
             "</html>\n";

        exit(0);
    }
    else
    {
        // The user does exist, he wants to login.

        if ($result[0]['password'] === hash('sha512', $result[0]['salt'].$_POST['password']))
        {
            $user = array("id" => (int)$result[0]['id'],
                          "role" => (int)$result[0]['role'],
                          "last_login" => $result[0]['last_login']);
        }
        else
        {
            echo "    <div>\n".
                 "      <p class=\"error\">\n".
                 "        Login failed.\n".
                 "      </p>\n".
                 "      <a href=\"index.php\">Retry</a>\n".
                 "    </div>\n".
                 "  </body>\n".
                 "</html>\n";

            exit(0);
        }
    }

    if (is_array($user) === true)
    {
        $_SESSION = array();

        $_SESSION['instance_path'] = str_replace("\\", "/", dirname(__FILE__));
        $_SESSION['user_id'] = $user['id'];
        $_SESSION['user_name'] = $_POST['name'];
        $_SESSION['user_role'] = $user['role'];

        require_once("./libraries/database.inc.php");

        if (Database::Get()->IsConnected() === true)
        {
            Database::Get()->ExecuteUnsecure("UPDATE `".Database::Get()->GetPrefix()."users`\n".
                                             "SET `last_login`=UTC_TIMESTAMP()\n".
                                             "WHERE `id`=".$user['id']);
        }

        echo "    <div>\n".
             "      <p class=\"success\">\n".
             "        Login was successful!\n".
             "      </p>\n".
             "      <div>\n".
             "        <a href=\"index.php\">Continue</a>\n".
             "      </div>\n".
             "    </div>\n";
    }

    echo "  </body>\n".
         "</html>\n";
}



?>
