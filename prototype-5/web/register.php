<?php
/* Copyright (C) 2019  Stephan Kreutzer
 *
 * This file is part of GBA.
 *
 * GBA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * GBA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with GBA. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/register.php
 * @author Stephan Kreutzer
 * @since 2019-09-04
 */



require_once("./libraries/https.inc.php");

echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n".
     "<!DOCTYPE html\n".
     "    PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"\n".
     "    \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n".
     "<html version=\"-//W3C//DTD XHTML 1.1//EN\" xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.w3.org/1999/xhtml http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd\" xml:lang=\"en\" lang=\"en\">\n".
     "  <head>\n".
     "    <meta http-equiv=\"content-type\" content=\"application/xhtml+xml; charset=UTF-8\"/>\n".
     "    <title>Register</title>\n".
     "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>\n".
     "    <style type=\"text/css\">\n".
     "      body\n".
     "      {\n".
     "          font-family: monospace;\n".
     "      }\n".
     "    </style>\n".
     "  </head>\n".
     "  <body>\n";

if (isset($_POST['name']) !== true ||
    isset($_POST['password']) !== true ||
    isset($_POST['email']) !== true)
{
    echo "    <div>\n".
         "      <h2>Register</h2>\n".
         "      <div>\n".
         "        <form action=\"register.php\" method=\"post\">\n".
         "          <fieldset>\n".
         "            <input type=\"text\" name=\"name\" size=\"20\" maxlength=\"60\"/> User name<br/>\n".
         "            <input type=\"password\" name=\"password\" size=\"20\" maxlength=\"60\"/> Password<br/>\n".
         "            <input type=\"text\" name=\"email\" size=\"20\" maxlength=\"255\"/> E-mail address<br/>\n".
         "            <input type=\"submit\" name=\"register\" value=\"Register\"/>\n".
         "          </fieldset>\n".
         "        </form>\n".
         "      </div>\n".
         "    </div>\n";
}
else
{
    $userName = "";
    $userPassword = "";
    $userEMail = "";

    if (isset($_POST['name']) === true)
    {
        $userName = $_POST['name'];
    }

    if (isset($_POST['password']) === true)
    {
        $userPassword = $_POST['password'];
    }

    if (isset($_POST['email']) === true)
    {
        $userEMail = $_POST['email'];
    }

    $successCreate = false;

    if (!empty($userName) &&
        !empty($userPassword) &&
        !empty($userEMail))
    {
        require_once(dirname(__FILE__)."/libraries/user_management.inc.php");

        if (Database::Get()->IsConnected() === true)
        {
            $id = insertNewUser($userName, $userPassword, $userEMail, USER_ROLE_USER);

            if ($id > 0)
            {
                $user = array("id" => $id);
                $successCreate = true;
            }
        }
    }

    if ($successCreate == true)
    {
        echo "    <p>\n".
             "      <span class=\"success\">User successfully created.</span>\n".
             "    </p>\n".
             "    <div>\n".
             "      <a href=\"index.php\">Login</a>\n".
             "    </div>\n";
    }
    else
    {
        echo "    <p>\n".
             "      <span class=\"error\">Database operation failed.</span>\n".
             "    </p>\n".
             "    <div>\n".
             "      <a href=\"register.php\">Retry</a>\n".
             "    </div>\n".
             "    <div>\n".
             "      <a href=\"index.php\">Back</a>\n".
             "    </div>\n";
    }
}

echo "  </body>\n".
     "</html>\n".
     "\n";


?>
