-- Copyright (C) 2019  Stephan Kreutzer
--
-- This file is part of GBA.
--
-- GBA is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License version 3 or any later version,
-- as published by the Free Software Foundation.
--
-- GBA is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- GNU Affero General Public License 3 for more details.
--
-- You should have received a copy of the GNU Affero General Public License 3
-- along with GBA. If not, see <http://www.gnu.org/licenses/>.



CREATE DATABASE `gba` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE gba;

CREATE USER 'gbauser'@'localhost' IDENTIFIED BY 'password';
GRANT USAGE ON *.* TO 'gbauser'@'localhost' IDENTIFIED BY 'password' WITH MAX_QUERIES_PER_HOUR 0
  MAX_CONNECTIONS_PER_HOUR 0
  MAX_UPDATES_PER_HOUR 0
  MAX_USER_CONNECTIONS 0;
GRANT ALL PRIVILEGES ON `gba`.* TO 'gbauser'@'localhost';


CREATE TABLE `best_next_steps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
