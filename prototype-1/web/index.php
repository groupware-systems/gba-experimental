<?php
/* Copyright (C) 2012-2019  Stephan Kreutzer
 *
 * This file is part of GBA.
 *
 * GBA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * GBA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with GBA. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/index.php
 * @author Stephan Kreutzer
 * @since 2019-07-22
 */



require_once("./libraries/database.inc.php");

echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n".
     "<!DOCTYPE html\n".
     "    PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"\n".
     "    \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n".
     "<html version=\"-//W3C//DTD XHTML 1.1//EN\" xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.w3.org/1999/xhtml http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd\" xml:lang=\"en\" lang=\"en\">\n".
     "  <head>\n".
     "    <meta http-equiv=\"content-type\" content=\"application/xhtml+xml; charset=UTF-8\"/>\n".
     "    <title>GBA</title>\n".
     "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>\n".
     "  </head>\n".
     "  <body>\n";

$errors = "";

if (isset($_POST['title']) === true)
{
    if (strlen($_POST['title']) <= 0)
    {
        $errors .= "<p>Error: Input field empty.</p>";
    }

    if (strlen($errors) <= 0)
    {
        if (Database::Get()->IsConnected() !== true)
        {
            $errors .= "<p>Error: Database isn't connected.</p>";
        }
    }

    if (strlen($errors) <= 0)
    {
        if (Database::Get()->BeginTransaction() !== true)
        {
            $errors .= "<p>Error: Can't begin database transaction.</p>";
        }
    }

    $id = -1;

    if (strlen($errors) <= 0)
    {
        $id = Database::Get()->Insert("INSERT INTO `".Database::Get()->GetPrefix()."best_next_steps` (`id`,\n".
                                      "    `title`)\n".
                                      "VALUES (?, ?)\n",
                                      array(NULL, $_POST['title']),
                                      array(Database::TYPE_NULL, Database::TYPE_STRING));

        if ($id <= 0)
        {
            Database::Get()->RollbackTransaction();

            $errors .= "<p>Error: Insertion failed.</p>";
        }
    }

    if (strlen($errors) <= 0)
    {
        if (Database::Get()->CommitTransaction() !== true)
        {
            $errors .= "<p>Error: Committing the transaction failed.</p>";
        }
    }
}

if (strlen($errors) > 0)
{
    echo "    <div>\n".
         "      ".$errors."\n".
         "    </div>\n";

    $errors = "";
}

echo "    <div>\n".
     "      <form action=\"index.php\" method=\"post\">\n".
     "        <fieldset>\n".
     "          <textarea name=\"title\" rows=\"24\" cols=\"80\"></textarea>\n".
     "          <input type=\"submit\" name=\"submit\"/>\n".
     "        </fieldset>\n".
     "      </form>\n".
     "    </div>\n";

$steps = array();

if (strlen($errors) <= 0)
{
    if (Database::Get()->IsConnected() !== true)
    {
        $errors .= "<p>Error: Database isn't connected.</p>";
    }

    if (strlen($errors) <= 0)
    {
        $steps = Database::Get()->QueryUnsecure("SELECT `id`,\n".
                                                 "    `title`\n".
                                                 "FROM `".Database::Get()->GetPrefix()."best_next_steps`\n".
                                                 "WHERE 1\n".
                                                 "ORDER BY `id` DESC");

        if (is_array($steps) !== true)
        {
            $errors .= "<p>Error: Database query failed.</p>";
        }
    }
}

if (strlen($errors) <= 0)
{
    if (count($steps) > 0)
    {
        echo "    <ul>\n";

        foreach ($steps as $step)
        {
            echo "      <li>".htmlspecialchars($step['title'], ENT_XHTML, "UTF-8")."</li>\n";
        }

        echo "    </ul>\n";
    }
}
else
{
    echo "    <div>\n".
         "      ".$errors."\n".
         "    </div>\n";

    $errors = "";
}

echo "  </body>\n".
     "</html>\n".
     "\n";


?>
