<?php
/* Copyright (C) 2012-2024 Stephan Kreutzer
 *
 * This file is part of GBA.
 *
 * GBA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * GBA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with GBA. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/entry.php
 * @author Stephan Kreutzer
 * @since 2019-08-26
 */



require_once("./libraries/https.inc.php");
require_once("./libraries/session.inc.php");
require_once("./libraries/user_defines.inc.php");


$typeSourceId = null;
$typeTargetId = null;

if (isset($_GET["type-source-id"]) === true)
{
    $typeSourceId = (int)$_GET["type-source-id"];
}

if (isset($_GET["type-target-id"]) === true)
{
    $typeTargetId = (int)$_GET["type-target-id"];
}

if ($typeSourceId === null &&
    $typeTargetId === null)
{
    http_response_code(400);
    exit(1);
}

if ($typeTargetId !== null &&
    $typeSourceId === null)
{
    $typeSourceId = $typeTargetId;
    $typeTargetId = null;
}

if ($typeSourceId !== null &&
    $typeTargetId !== null)
{
    if ($typeSourceId == $typeTargetId)
    {
        $typeTargetId = null;
    }
}

/** @todo Handle case of no types being configured. Also check if given types exist, for 404. */


require_once("./libraries/database.inc.php");

if (Database::Get()->IsConnected() !== true)
{
    http_response_code(500);
    exit(-1);
}

if (Database::Get()->BeginTransaction() !== true)
{
    http_response_code(500);
    exit(-1);
}


$idEntryTarget = null;

if (defined("SESSION_ACTIVE") === true)
{
    if ((isset($_POST["text"]) === true ||
         isset($_POST["label"]) === true) &&
        isset($_POST["submit"]) === true)
    {
        $updateText = true;

        if ($_POST["submit"] === "Add")
        {
            $idEntryNew = null;

            if (isset($_POST["parent-id"]) === true)
            {
                if ($typeTargetId === null ||
                    $typeSourceId == null)
                {
                    Database::Get()->RollbackTransaction();
                    http_response_code(400);
                    exit(1);
                }
            }
            else
            {
                if ($typeSourceId === null)
                {
                    Database::Get()->RollbackTransaction();
                    http_response_code(400);
                    exit(1);
                }
            }

            if (isset($_POST["parent-id"]) === true)
            {

                $idEntryNew = Database::Get()->Insert("INSERT INTO `".Database::Get()->GetPrefix()."node` (`id`,\n".
                                                      "    `id_type`,\n".
                                                      "    `ordinal`)\n".
                                                      "VALUES (?, ?, 0)",
                                                      array(NULL, $typeTargetId),
                                                      array(Database::TYPE_NULL, Database::TYPE_INT));
            }
            else
            {
                $idEntryNew = Database::Get()->Insert("INSERT INTO `".Database::Get()->GetPrefix()."node` (`id`,\n".
                                                      "    `id_type`,\n".
                                                      "    `ordinal`)\n".
                                                      "VALUES (?, ?, 0)",
                                                      array(NULL, $typeSourceId),
                                                      array(Database::TYPE_NULL, Database::TYPE_INT));
            }

            if ($idEntryNew <= 0)
            {
                Database::Get()->RollbackTransaction();
                http_response_code(500);
                exit(-1);
            }

            $idEntryTarget = $idEntryNew;

            if (isset($_POST["parent-id"]) === true)
            {
                $labelValue = null;
                $labelType = Database::TYPE_NULL;

                if (isset($_POST["label"]) === true)
                {
                    $labelValue = $_POST["label"];
                    $labelType = Database::TYPE_STRING;
                }

                $idEntryEdge = Database::Get()->Insert("INSERT INTO `".Database::Get()->GetPrefix()."edge` (`id`,\n".
                                                       "    `id_node_source`,\n".
                                                       "    `id_type_source`,\n".
                                                       "    `id_node_target`,\n".
                                                       "    `id_type_target`,\n".
                                                       "    `label`)\n".
                                                       "VALUES (?, ?, ?, ?, ?, ?)",
                                                       array(NULL, $idEntryNew, $typeSourceId, (int)$_POST["parent-id"], $typeTargetId, $labelValue),
                                                       array(Database::TYPE_NULL, Database::TYPE_INT, Database::TYPE_INT, Database::TYPE_INT, Database::TYPE_INT, $labelType));

                if ($idEntryEdge <= 0)
                {
                    Database::Get()->RollbackTransaction();
                    http_response_code(500);
                    exit(-1);
                }
            }
        }
        else if ($_POST["submit"] === "Update")
        {
            $updateText = false;

            if (isset($_POST["node-id"]) !== true)
            {
                Database::Get()->RollbackTransaction();
                http_response_code(400);
                exit(0);
            }

            if (isset($_POST["text"]) === true)
            {
                $previousText = Database::Get()->Query("SELECT `text`\n".
                                                       "FROM `".Database::Get()->GetPrefix()."node_revision`\n".
                                                       "WHERE `id_node`=?\n".
                                                       "ORDER BY `revision_datetime` DESC\n".
                                                       "LIMIT 1",
                                                       array((int)$_POST["node-id"]),
                                                       array(Database::TYPE_INT));

                if (is_array($previousText) !== true)
                {
                    Database::Get()->RollbackTransaction();
                    http_response_code(500);
                    exit(-1);
                }

                if (count($previousText) <= 0)
                {
                    Database::Get()->RollbackTransaction();
                    http_response_code(404);
                    exit(1);
                }

                if ($previousText[0]["text"] != $_POST["text"])
                {
                    $updateText = true;
                    $idEntryTarget = (int)$_POST["node-id"];
                }
                else
                {
                    $updateText = false;
                }
            }

            $updateLabel = false;
            $updateLabelEdgeId = null;

            if (isset($_POST["label"]) === true &&
                $typeSourceId != null &&
                $typeTargetId != null &&
                isset($_POST["parent-id"]) === true)
            {
                $previousLabel = Database::Get()->Query("SELECT `id`,\n".
                                                        "    `label`\n".
                                                        "FROM `".Database::Get()->GetPrefix()."edge`\n".
                                                        "WHERE ((`id_type_target`=? AND\n".
                                                        "      `id_type_source`=?) OR\n".
                                                        "     (`id_type_target`=? AND\n".
                                                        "      `id_type_source`=?)) AND\n".
                                                        "    ((`id_node_source`=? AND\n".
                                                        "      `id_node_target`=?) OR\n".
                                                        "     (`id_node_source`=? AND\n".
                                                        "      `id_node_target`=?))",
                                                        array($typeTargetId, $typeSourceId, $typeSourceId, $typeTargetId, (int)$_POST["parent-id"], (int)$_POST["node-id"], (int)$_POST["node-id"], (int)$_POST["parent-id"]),
                                                        array(Database::TYPE_INT, Database::TYPE_INT, Database::TYPE_INT, Database::TYPE_INT, Database::TYPE_INT, Database::TYPE_INT, Database::TYPE_INT, Database::TYPE_INT));

                if (is_array($previousLabel) !== true)
                {
                    Database::Get()->RollbackTransaction();
                    http_response_code(500);
                    exit(-1);
                }

                if (count($previousLabel) <= 0)
                {
                    Database::Get()->RollbackTransaction();
                    http_response_code(404);
                    exit(1);
                }

                if ($previousLabel[0]["label"] != $_POST["label"])
                {
                    $updateLabelEdgeId = (int)$previousLabel[0]["id"];
                    $updateLabel = true;
                }
                else
                {
                    $updateLabel= false;
                }
            }

            if ($updateLabel === true &&
                $updateLabelEdgeId !== null)
            {
                $success = Database::Get()->Execute("UPDATE `".Database::Get()->GetPrefix()."edge`\n".
                                                    "SET `label`=?\n".
                                                    "WHERE `id`=?",
                                                    array($_POST["label"], $updateLabelEdgeId),
                                                    array(Database::TYPE_STRING, Database::TYPE_INT));

                if ($success !== true)
                {
                    Database::Get()->RollbackTransaction();
                    http_response_code(500);
                    exit(-1);
                }
            }
        }
        else
        {
            Database::Get()->RollbackTransaction();
            http_response_code(400);
            exit(0);
        }

        if ($updateText === true)
        {
            $idEntryRevision = Database::Get()->Insert("INSERT INTO `".Database::Get()->GetPrefix()."node_revision` (`id`,\n".
                                                       "    `text`,\n".
                                                       "    `revision_datetime`,\n".
                                                       "    `id_user`,\n".
                                                       "    `id_node`)\n".
                                                       "VALUES (?, ?, UTC_TIMESTAMP(), ?, ?)",
                                                       array(NULL, $_POST['text'], $_SESSION['user_id'], $idEntryTarget),
                                                       array(Database::TYPE_NULL, Database::TYPE_STRING, Database::TYPE_INT, Database::TYPE_INT));

            if ($idEntryRevision <= 0)
            {
                Database::Get()->RollbackTransaction();
                http_response_code(500);
                exit(-1);
            }
        }
    }
}

$entriesSource = null;

if ($typeSourceId !== null)
{
    $entriesSource = Database::Get()->Query("SELECT `".Database::Get()->GetPrefix()."node`.`id` AS `node_id`,\n".
                                            "    `".Database::Get()->GetPrefix()."node`.`id_type` AS `node_id_type`,\n".
                                            "    `".Database::Get()->GetPrefix()."node`.`ordinal` AS `node_ordinal`,\n".
                                            "    `".Database::Get()->GetPrefix()."node_revision`.`id` AS `node_revision_id`,\n".
                                            "    `".Database::Get()->GetPrefix()."node_revision`.`text` AS `node_revision_text`,\n".
                                            "    `".Database::Get()->GetPrefix()."node_revision`.`revision_datetime` AS `node_revision_revision_datetime`,\n".
                                            "    `".Database::Get()->GetPrefix()."node_revision`.`id_user` AS `node_revision_id_user`,\n".
                                            "    `".Database::Get()->GetPrefix()."user`.`name` AS `user_name`\n".
                                            "FROM `".Database::Get()->GetPrefix()."node_revision`\n".
                                            "INNER JOIN `".Database::Get()->GetPrefix()."node` ON\n".
                                            "    `".Database::Get()->GetPrefix()."node`.`id` =\n".
                                            "    `".Database::Get()->GetPrefix()."node_revision`.`id_node`\n".
                                            "INNER JOIN `".Database::Get()->GetPrefix()."user` ON\n".
                                            "    `".Database::Get()->GetPrefix()."user`.`id` =\n".
                                            "    `".Database::Get()->GetPrefix()."node_revision`.`id_user`\n".
                                            "WHERE `".Database::Get()->GetPrefix()."node`.`id_type`=?\n".
                                            "ORDER BY `".Database::Get()->GetPrefix()."node`.`ordinal` ASC,\n".
                                            "    `".Database::Get()->GetPrefix()."node`.`id` ASC,\n".
                                            "    `".Database::Get()->GetPrefix()."node_revision`.`revision_datetime` DESC",
                                            array($typeSourceId),
                                            array(Database::TYPE_INT));

    if (is_array($entriesSource) !== true)
    {
        Database::Get()->RollbackTransaction();
        http_response_code(500);
        exit(-1);
    }
}

$entriesTarget = null;

if ($typeTargetId !== null)
{
    $entriesTarget = Database::Get()->Query("SELECT `".Database::Get()->GetPrefix()."node`.`id` AS `node_id`,\n".
                                            "    `".Database::Get()->GetPrefix()."node`.`id_type` AS `node_id_type`,\n".
                                            "    `".Database::Get()->GetPrefix()."node`.`ordinal` AS `node_ordinal`,\n".
                                            "    `".Database::Get()->GetPrefix()."node_revision`.`id` AS `node_revision_id`,\n".
                                            "    `".Database::Get()->GetPrefix()."node_revision`.`text` AS `node_revision_text`,\n".
                                            "    `".Database::Get()->GetPrefix()."node_revision`.`revision_datetime` AS `node_revision_revision_datetime`,\n".
                                            "    `".Database::Get()->GetPrefix()."node_revision`.`id_user` AS `node_revision_id_user`,\n".
                                            "    `".Database::Get()->GetPrefix()."user`.`name` AS `user_name`\n".
                                            "FROM `".Database::Get()->GetPrefix()."node_revision`\n".
                                            "INNER JOIN `".Database::Get()->GetPrefix()."node` ON\n".
                                            "    `".Database::Get()->GetPrefix()."node`.`id` =\n".
                                            "    `".Database::Get()->GetPrefix()."node_revision`.`id_node`\n".
                                            "INNER JOIN `".Database::Get()->GetPrefix()."user` ON\n".
                                            "    `".Database::Get()->GetPrefix()."user`.`id` =\n".
                                            "    `".Database::Get()->GetPrefix()."node_revision`.`id_user`\n".
                                            "WHERE `".Database::Get()->GetPrefix()."node`.`id_type`=?\n".
                                            "ORDER BY `".Database::Get()->GetPrefix()."node`.`ordinal` ASC,\n".
                                            "    `".Database::Get()->GetPrefix()."node`.`id` ASC,\n".
                                            "    `".Database::Get()->GetPrefix()."node_revision`.`revision_datetime` DESC",
                                            array($typeTargetId),
                                            array(Database::TYPE_INT));

    if (is_array($entriesTarget) !== true)
    {
        Database::Get()->RollbackTransaction();
        http_response_code(500);
        exit(-1);
    }
}

$edges = null;

if ($typeSourceId !== null &&
    $typeTargetId !== null)
{
    $edges = Database::Get()->Query("SELECT `id`,\n".
                                    "    `id_node_source`,\n".
                                    "    `id_type_source`,\n".
                                    "    `id_node_target`,\n".
                                    "    `id_type_target`,\n".
                                    "    `label`\n".
                                    "FROM `".Database::Get()->GetPrefix()."edge`\n".
                                    "WHERE (`id_type_source`=? AND\n".
                                    "     `id_type_target`=?) OR\n".
                                    "    (`id_type_source`=? AND\n".
                                    "     `id_type_target`=?)",
                                    array($typeSourceId, $typeTargetId, $typeTargetId, $typeSourceId),
                                    array(Database::TYPE_INT, Database::TYPE_INT, Database::TYPE_INT, Database::TYPE_INT));

    if (is_array($edges) !== true)
    {
        Database::Get()->RollbackTransaction();
        http_response_code(500);
        exit(-1);
    }
}


/** @todo Check if inserted node is found inside queried result
  * and abort with error if not? */

if (Database::Get()->CommitTransaction() !== true)
{
    Database::Get()->RollbackTransaction();
    http_response_code(500);
    exit(-1);
}


$tree = array();

for ($i = 0, $max = count($entriesSource); $i < $max; $i++)
{
    $sourceId = (int)$entriesSource[$i]["node_id"];

    // Continue? As revisions may repeat the node?
    if (array_key_exists($sourceId, $tree) == true)
    {

    }

    $tree[$sourceId] = array();
}

if ($edges != null)
{
    for ($i = 0, $max = count($edges); $i < $max; $i++)
    {
        $edgeSourceId = (int)$edges[$i]["id_node_source"];
        $edgeTargetId = (int)$edges[$i]["id_node_target"];

        if ($edgeSourceId == $edgeTargetId)
        {

        }

        // Reverse connection is implicit for each edge.

        if (array_key_exists($edgeSourceId, $tree) == true)
        {
            if (isset($tree[$edgeSourceId][$edgeTargetId]) == true)
            {
                http_response_code(500);
                exit(-1);
            }

            $tree[$edgeSourceId][$edgeTargetId] = array("id" => $edgeTargetId,
                                                        "label" => $edges[$i]["label"]);
        }

        if (array_key_exists($edgeTargetId, $tree) == true)
        {
            if (isset($tree[$edgeTargetId][$edgeSourceId]) == true)
            {
                http_response_code(500);
                exit(-1);
            }

            $tree[$edgeTargetId][$edgeSourceId] = array("id" => $edgeSourceId,
                                                        "label" => $edges[$i]["label"]);
        }
    }
}


/** @todo Title + header! */
echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n".
     "<!DOCTYPE html\n".
     "    PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"\n".
     "    \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n".
     "<html version=\"-//W3C//DTD XHTML 1.1//EN\" xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.w3.org/1999/xhtml http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd\" xml:lang=\"en\" lang=\"en\">\n".
     "  <head>\n".
     "    <meta http-equiv=\"content-type\" content=\"application/xhtml+xml; charset=UTF-8\"/>\n".
     "    <title>Entry</title>\n".
     "    <style type=\"text/css\">\n".
     "      body\n".
     "      {\n".
     "          font-family: monospace;\n".
     "      }\n".
     "\n".
     "      .deleted\n".
     "      {\n".
     "          display: none;\n".
     "      }\n".
     "\n".
     "      .edge-label\n".
     "      {\n".
     "          background-color: #BEBEBE;\n".
     "      }\n".
     "    </style>\n".
     "    <script type=\"text/javascript\" src=\"entry_controls.js\"></script>\n".
     "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>\n".
     "  </head>\n".
     "  <body>\n".
     "    <div>\n".
    "      <ul id=\"list-level-top\">\n";

$entryCounter = 0;

if (count($entriesSource) > 0)
{
    $lastEntryId = -1;

    foreach ($entriesSource as $entry)
    {
        $entryId = (int)$entry['node_id'];

        if ($lastEntryId != $entryId)
        {
            if ($lastEntryId >= 0)
            {
                echo "              </tbody>\n".
                     "            </table>\n".
                     "          </div>\n";

                echoTargetList($lastEntryId,
                               $tree[$lastEntryId],
                               $entriesTarget,
                               $entryCounter,
                               $typeSourceId,
                               $typeTargetId);

                echo "        </li>\n";
            }

            $lastEntryId = $entryId;
            $entryCounter += 1;

            $deleted = "";

            if (strlen($entry['node_revision_text']) <= 0)
            {
                $deleted = " class=\"deleted\"";
            }

            echo "        <li".$deleted.">\n".
                 "          <div id=\"update-control-".$entryCounter."\"></div>\n".
                 "          <span id=\"entry-text-".$entryCounter."\">".htmlspecialchars($entry['node_revision_text'], ENT_XHTML, "UTF-8")."</span>";

            if (defined("SESSION_ACTIVE") === true)
            {
                $functionParameters = $entryId.", ".$entryCounter.", ".$typeSourceId;

                if ($typeTargetId != null)
                {
                    $functionParameters .= ", ".$typeTargetId;
                }
                else
                {
                    $functionParameters .= ", -1";
                }

                $functionParameters .= ", -1";

                echo "<span id=\"update-control-link-".$entryCounter."\"> <a href=\"#\" onclick=\"LoadEditControl(".$functionParameters."); return false;\">Edit</a></span>";
            }

            echo " <a href=\"#\" onclick=\"ToggleRevisions('revisions-".$entryCounter."'); return false;\">Revisions</a>\n".
                 "          <div id=\"revisions-".$entryCounter."\" style=\"display: none;\">\n".
                 "            <table border=\"1\">\n".
                 "              <thead>\n".
                 "                <tr>\n".
                 "                  <th>Timestamp (UTC)</th>\n".
                 "                  <th>Author</th>\n".
                 "                  <th>Version</th>\n".
                 "                </tr>\n".
                 "              </thead>\n".
                 "              <tbody>\n";
        }

        echo "                <tr>\n".
             "                  <td>".str_replace(" ", "T", $entry['node_revision_revision_datetime'])."Z</td>\n".
             "                  <td>".htmlspecialchars($entry['user_name'], ENT_XHTML, "UTF-8")."</td>\n".
             "                  <td>".htmlspecialchars($entry['node_revision_text'], ENT_XHTML, "UTF-8")."</td>\n".
             "                </tr>\n";
    }

    if ($lastEntryId >= 0)
    {
        echo "              </tbody>\n".
             "            </table>\n".
             "          </div>\n";

        echoTargetList($lastEntryId,
                       $tree[$lastEntryId],
                       $entriesTarget,
                       $entryCounter,
                       $typeSourceId,
                       $typeTargetId);

        echo "        </li>\n";
    }
}

if (defined("SESSION_ACTIVE") === true)
{
    echo "        <li class=\"add_control\">\n".
         "          <div style=\"display: list-item;\"><a href=\"#\" onclick=\"LoadAddControl(-1, -1, ".$typeSourceId.", ".$typeTargetId."); return false;\">Add</a></div>\n".
         "        </li>\n";
}

echo "      </ul>\n".
     "      <hr/>\n".
     "      <p>\n";

require_once("./license.inc.php");

echo "        ".
     getContentLicenseStringPrefix();

echoCredits($entriesSource, $entriesTarget, $tree);

echo " – ".
     getContentLicenseStringPostfix().
     " ".
     getSoftwareLicenseString()."\n".
     "      </p>\n".
     "    </div>\n".
     "  </body>\n".
     "</html>\n".
     "\n";



function echoTargetList($sourceId,
                        &$children,
                        &$entriesTarget,
                        &$entryCounter,
                        $typeSourceId,
                        $typeTargetId)
{
    $levelId = $entryCounter;

    $max = count($children);

    if ($max <= 0)
    {
        if (defined("SESSION_ACTIVE") === true)
        {
            echo "          <span id=\"list-level-".$entryCounter."\">\n".
                 "            <span class=\"add_control\">\n".
                 "              <a href=\"#\" onclick=\"LoadAddControl(".$sourceId.", ".$levelId.", ".$typeSourceId.", ".$typeTargetId."); return false;\">Add</a>\n".
                 "            </span>\n".
                 "          </span\n";
        }

        return 1;
    }

    $childrenIds = array_keys($children);

    echo "          <div>\n".
         "            <ul id=\"list-level-".$entryCounter."\">\n";

    $lastEntryId = -1;

    foreach ($entriesTarget as $entry)
    {
        $entryId = (int)$entry['node_id'];

        if (in_array($entryId, $childrenIds) != true)
        {
            continue;
        }

        if ($lastEntryId != $entryId)
        {
            if ($lastEntryId >= 0)
            {
                echo "                    </tbody>\n".
                     "                  </table>\n".
                     "                </div>\n".
                     "              </li>\n";
            }

            $lastEntryId = $entryId;
            $entryCounter += 1;

            $deleted = "";

            if (strlen($entry['node_revision_text']) <= 0)
            {
                $deleted = " class=\"deleted\"";
            }

            echo "              <li".$deleted.">\n".
                 "                <div id=\"update-control-".$entryCounter."\"></div>\n".
                 "                ";

            if ($children[$entryId]["label"] != null)
            {
                echo "<span class=\"edge-label\" id=\"edge-label-text-".$entryCounter."\">".htmlspecialchars($children[$entryId]["label"], ENT_XHTML, "UTF-8")."</span> ";
            }

            echo "<span id=\"entry-text-".$entryCounter."\">".htmlspecialchars($entry['node_revision_text'], ENT_XHTML, "UTF-8")."</span>";

            if (defined("SESSION_ACTIVE") === true)
            {
                $functionParameters = $entryId.", ".$entryCounter.", ".$typeSourceId;

                if ($typeTargetId != null)
                {
                    $functionParameters .= ", ".$typeTargetId;
                }
                else
                {
                    $functionParameters .= ", -1";
                }

                if ($sourceId > 0)
                {
                    $functionParameters .= ", ".$sourceId;
                }
                else
                {
                    $functionParameters .= ", -1";
                }

                echo "<span id=\"update-control-link-".$entryCounter."\"> <a href=\"#\" onclick=\"LoadEditControl(".$functionParameters."); return false;\">Edit</a></span>";
            }

            echo " <a href=\"#\" onclick=\"ToggleRevisions('revisions-".$entryCounter."'); return false;\">Revisions</a>\n".
                 "                <div id=\"revisions-".$entryCounter."\" style=\"display: none;\">\n".
                 "                  <table border=\"1\">\n".
                 "                    <thead>\n".
                 "                      <tr>\n".
                 "                        <th>Timestamp (UTC)</th>\n".
                 "                        <th>Author</th>\n".
                 "                        <th>Version</th>\n".
                 "                      </tr>\n".
                 "                    </thead>\n".
                 "                    <tbody>\n";
        }

        echo "                      <tr>\n".
             "                        <td>".str_replace(" ", "T", $entry['node_revision_revision_datetime'])."Z</td>\n".
             "                        <td>".htmlspecialchars($entry['user_name'], ENT_XHTML, "UTF-8")."</td>\n".
             "                        <td>".htmlspecialchars($entry['node_revision_text'], ENT_XHTML, "UTF-8")."</td>\n".
             "                      </tr>\n";
    }

    if ($lastEntryId >= 0)
    {
        echo "                    </tbody>\n".
             "                  </table>\n".
             "                </div>\n".
             "              </li>\n";
    }

    if (defined("SESSION_ACTIVE") === true)
    {
        echo "              <li class=\"add_control\">\n".
             "                <div style=\"display: list-item;\"><a href=\"#\" onclick=\"LoadAddControl(".$sourceId.", ".$levelId.", ".$typeSourceId.", ".$typeTargetId."); return false;\">Add</a></div>\n".
             "              </li>\n";
    }

    echo "            </ul>\n".
         "          </div>\n";
}

function echoCredits(&$entriesSource, &$entriesTarget, &$tree)
{
    if (empty($entriesSource) == true)
    {
        return null;
    }

    $iso8601Pattern = DateTimeInterface::ISO8601;

    if (version_compare(PHP_VERSION, "8.2") >= 0)
    {
        $iso8601Pattern = DateTimeInterface::ISO8601_EXPANDED;
    }

    $contributors = array();
    $years = array();
    $fallbackYear = (int)date("Y");

    $lastEntryId = -1;

    for ($i = count($entriesSource) - 1; $i >= 0; $i--)
    {
        $contributors[$entriesSource[$i]["user_name"]] = true;

        {
            $yearInFormat = str_replace(" ", "T", $entriesSource[$i]["node_revision_revision_datetime"])."Z";
            $year = DateTime::createFromFormat($iso8601Pattern, $yearInFormat)->format("o");

            if ($year !== false)
            {
                $year = (int)$year;

                if (isset($years[$year]) == true)
                {
                    if ($years[$year] != false)
                    {
                        $years[$year] = true;
                    }
                }
                else
                {
                    $years[$year] = true;
                }
            }
            else
            {
                $years[$fallbackYear] = false;
            }
        }

        if ((int)$entriesSource[$i]["node_id"] != $lastEntryId)
        {
            if (array_key_exists((int)$entriesSource[$i]["node_id"], $tree) == true)
            {
                $children = $tree[(int)$entriesSource[$i]["node_id"]];

                foreach (array_reverse($children) as $child)
                {
                    // OK, all of this is ugly, only there because there's no
                    // node by ID lookup (at the expense of memory), but point
                    // being, to get away with it for the time being, quick and dirty.

                    $targetId = null;

                    for ($k = count($entriesTarget) - 1; $k >= 0; $k--)
                    {
                        if ((int)$entriesTarget[$k]["node_id"] == $child["id"])
                        {
                            $targetId = (int)$entriesTarget[$k]["node_id"];


                            $contributors[$entriesTarget[$k]["user_name"]] = true;

                            $yearInFormat = str_replace(" ", "T", $entriesTarget[$k]["node_revision_revision_datetime"])."Z";
                            $year = DateTime::createFromFormat($iso8601Pattern, $yearInFormat)->format("o");

                            if ($year !== false)
                            {
                                $year = (int)$year;

                                if (isset($years[$year]) == true)
                                {
                                    if ($years[$year] != false)
                                    {
                                        $years[$year] = true;
                                    }
                                }
                                else
                                {
                                    $years[$year] = true;
                                }
                            }
                            else
                            {
                                $years[$fallbackYear] = false;
                            }
                        }
                        else
                        {
                            if ($targetId != null)
                            {
                                // No need to record the same target entry twice.
                                break;
                            }
                        }
                    }
                }
            }

            $lastEntryId = (int)$entriesSource[$i]["node_id"];
        }
    }

    ksort($years);

    {
        $yearsIndexed = array();
        $correct = true;

        if (count($years) > 0)
        {
            foreach ($years as $year => $correct)
            {
                $yearsIndexed[] = array("year" => $year,
                                        "correct" => $correct);

                if ($correct != true)
                {
                    $correct = false;
                }
            }
        }

        $max = count($yearsIndexed);

        if ($max >= 2)
        {
            echo $yearsIndexed[0]["year"]."-".$yearsIndexed[$max-1]["year"];
        }
        else
        {
            echo $yearsIndexed[0]["year"];
        }

        echo " ";

        if ($correct != true)
        {
            echo "(?) ";
        }
    }


    $contributors = array_keys($contributors);

    for ($i = 0, $max = count($contributors); $i < $max; $i++)
    {
        if ($i > 0)
        {
            echo ", ";
        }

        echo htmlspecialchars($contributors[$i], ENT_XHTML, "UTF-8");
    }
}



?>
