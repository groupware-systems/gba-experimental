/* Copyright (C) 2019-2024 Stephan Kreutzer
 *
 * This file is part of GBA.
 *
 * GBA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * GBA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with GBA. If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

function LoadEditControl(nodeId, entryId, typeSourceId, typeTargetId, nodeParentId)
{
    let updateControlDiv = document.getElementById('update-control-' + entryId);
    let updateControlLink = document.getElementById('update-control-link-' + entryId);
    let entryTextSpan = document.getElementById("entry-text-" + entryId);

    if (updateControlDiv == null ||
        entryTextSpan == null)
    {
        return -1;
    }

    for (let i = updateControlDiv.childNodes.length - 1; i >= 0; i--)
    {
        removeNode(updateControlDiv.childNodes[i], updateControlDiv);
    }

    {
        let formUrl = "entry.php?type-source-id=" + typeSourceId;

        if (typeTargetId > 0)
        {
            formUrl += "&type-target-id=" + typeTargetId;
        }

        let form = document.createElement("form");
        form.setAttribute("method", "post");
        form.setAttribute("action", formUrl);

        let fieldset = document.createElement("fieldset");

        let textarea = document.createElement("textarea");
        textarea.setAttribute("name", "text");
        textarea.setAttribute("rows", "1");
        textarea.setAttribute("cols", "80");

        {
            let textareaText = document.createTextNode(entryTextSpan.innerText);
            textarea.appendChild(textareaText);
        }

        let submit = document.createElement("input");
        submit.setAttribute("type", "submit");
        submit.setAttribute("name", "submit");
        submit.setAttribute("value", "Update");

        let cancel = document.createElement("input");
        cancel.setAttribute("type", "button");
        cancel.setAttribute("onclick", "UnloadEditControl(" + entryId + ");");
        cancel.setAttribute("value", "Cancel");

        let nodeIdHidden = document.createElement("input");
        nodeIdHidden.setAttribute("type", "hidden");
        nodeIdHidden.setAttribute("name", "node-id");
        nodeIdHidden.setAttribute("value", nodeId);
        fieldset.appendChild(nodeIdHidden);

        fieldset.appendChild(textarea);

        if (nodeParentId > 0)
        {
            fieldset.appendChild(document.createElement("br"));

            let nodeParentIdHidden = document.createElement("input");
            nodeParentIdHidden.setAttribute("type", "hidden");
            nodeParentIdHidden.setAttribute("name", "parent-id");
            nodeParentIdHidden.setAttribute("value", nodeParentId);
            fieldset.appendChild(nodeParentIdHidden);

            let labelText = document.getElementById("edge-label-text-" + entryId);

            if (labelText != null)
            {
                labelText = labelText.innerText;
            }

            let label = document.createElement("input");
            label.setAttribute("type", "text");
            label.setAttribute("name", "label");

            if (labelText != null)
            {
                label.setAttribute("value", labelText);
            }

            fieldset.appendChild(label);
        }

        fieldset.appendChild(submit);
        fieldset.appendChild(cancel);

        form.appendChild(fieldset);
        updateControlDiv.appendChild(form);
    }

    /*
    {
        let form = document.createElement("form");
        form.setAttribute("method", "post");
        form.setAttribute("action", "entry.php?id=" + entryId);

        let fieldset = document.createElement("fieldset");

        let label = document.createElement("label");
        label.setAttribute("for", "parent-id");
        label.appendChild(document.createTextNode("Parent-ID"));

        let textarea = document.createElement("textarea");
        textarea.setAttribute("id", "parent-id");
        textarea.setAttribute("name", "parent-id");
        textarea.setAttribute("rows", "1");
        textarea.setAttribute("cols", "8");

        let submit = document.createElement("input");
        submit.setAttribute("type", "submit");
        submit.setAttribute("name", "submit");
        submit.setAttribute("value", "Update");

        let cancel = document.createElement("input");
        cancel.setAttribute("type", "button");
        cancel.setAttribute("onclick", "UnloadEditControl(" + entryId + ");");
        cancel.setAttribute("value", "Cancel");

        fieldset.appendChild(label);
        fieldset.appendChild(textarea);
        fieldset.appendChild(submit);
        fieldset.appendChild(cancel);

        form.appendChild(fieldset);
        updateControlDiv.appendChild(form);
    }
    */

    {
        let copyright = document.createElement("p");
        copyright.appendChild(document.createTextNode("By submitting a form, you confirm that you’re the author of the text/setting, or possess compatible permission to license and publish your input under the "));

        {
            let link = document.createElement("a");
            link.setAttribute("href", "https://creativecommons.org/licenses/by-sa/4.0/legalcode");
            link.appendChild(document.createTextNode("Creative Commons Attribution-ShareAlike 4.0 International"));
            copyright.appendChild(link);
        }

        copyright.appendChild(document.createTextNode("."));
        updateControlDiv.appendChild(copyright);
    }

    if (updateControlLink != null)
    {
        updateControlLink.setAttribute("style", "display: none;");
    }

    return 0;
}

function UnloadEditControl(entryId)
{
    let updateControlDiv = document.getElementById('update-control-' + entryId);
    let updateControlLink = document.getElementById('update-control-link-' + entryId);
    let entryTextSpan = document.getElementById("entry-text-" + entryId);

    if (updateControlDiv == null ||
        entryTextSpan == null)
    {
        return -1;
    }

    for (let i = updateControlDiv.childNodes.length - 1; i >= 0; i--)
    {
        removeNode(updateControlDiv.childNodes[i], updateControlDiv);
    }

    if (updateControlLink != null)
    {
        updateControlLink.setAttribute("style", "display: inline;");
    }
}

function LoadAddControl(nodeParentId, entryId, idTypeSource, idTypeTarget)
{
    let container = null;

    if (entryId == -1)
    {
        container = document.getElementById("list-level-top");
    }
    else
    {
        container = document.getElementById("list-level-" + entryId);
    }

    if (container == null)
    {
        return -1;
    }

    let target = null;

    for (let i = 0, max = container.childNodes.length; i < max; i++)
    {
        if (container.childNodes.item(i).nodeType != Node.ELEMENT_NODE)
        {
            continue;
        }

        if (container.childNodes.item(i).classList.contains("add_control") == true)
        {
            while (container.childNodes.item(i).hasChildNodes() == true)
            {
                removeNode(container.childNodes.item(i).lastChild, container.childNodes.item(i));
            }

            if (target != null)
            {

            }

            target = container.childNodes.item(i);
        }
    }

    if (target == null)
    {

    }


    {
        let form = document.createElement("form");
        form.setAttribute("method", "post");
        form.setAttribute("action", "entry.php?type-source-id=" + idTypeSource + "&type-target-id=" + idTypeTarget);

        let fieldset = document.createElement("fieldset");

        let textarea = document.createElement("textarea");
        textarea.setAttribute("name", "text");
        textarea.setAttribute("rows", "1");
        textarea.setAttribute("cols", "80");

        let submit = document.createElement("input");
        submit.setAttribute("type", "submit");
        submit.setAttribute("name", "submit");
        submit.setAttribute("value", "Add");

        let cancel = document.createElement("input");
        cancel.setAttribute("type", "button");
        cancel.setAttribute("onclick", "UnloadAddControl(" + nodeParentId + ", " + entryId + ", " + idTypeSource + ", " + idTypeTarget + ");");
        cancel.setAttribute("value", "Cancel");

        let copyright = document.createElement("p");
        copyright.appendChild(document.createTextNode("By submitting the form, you confirm that you’re the author of the text or possess compatible permission to license and publish your input under the "));

        {
            let link = document.createElement("a");
            link.setAttribute("href", "https://creativecommons.org/licenses/by-sa/4.0/legalcode");
            link.appendChild(document.createTextNode("Creative Commons Attribution-ShareAlike 4.0 International"));
            copyright.appendChild(link);
        }

        copyright.appendChild(document.createTextNode("."));

        if (nodeParentId > 0)
        {
            let nodeIdHidden = document.createElement("input");
            nodeIdHidden.setAttribute("type", "hidden");
            nodeIdHidden.setAttribute("name", "parent-id");
            nodeIdHidden.setAttribute("value", nodeParentId);
            fieldset.appendChild(nodeIdHidden);
        }

        fieldset.appendChild(textarea);

        if (nodeParentId > 0)
        {
            let label = document.createElement("input");
            label.setAttribute("type", "text");
            label.setAttribute("name", "label");

            fieldset.appendChild(document.createElement("br"));
            fieldset.appendChild(label);
        }

        fieldset.appendChild(submit);
        fieldset.appendChild(cancel);
        fieldset.appendChild(copyright);

        form.appendChild(fieldset);
        target.appendChild(form);
    }

    return 0;
}

function UnloadAddControl(nodeParentId, entryId, idTypeSource, idTypeTarget)
{
    let container = null;

    if (entryId == -1)
    {
        container = document.getElementById("list-level-top");
    }
    else
    {
        container = document.getElementById("list-level-" + entryId);
    }

    if (container == null)
    {
        return -1;
    }

    let target = null;

    for (let i = 0, max = container.childNodes.length; i < max; i++)
    {
        if (container.childNodes.item(i).nodeType != Node.ELEMENT_NODE)
        {
            continue;
        }

        if (container.childNodes.item(i).classList.contains("add_control") == true)
        {
            while (container.childNodes.item(i).hasChildNodes() == true)
            {
                removeNode(container.childNodes.item(i).lastChild, container.childNodes.item(i));
            }

            if (target != null)
            {

            }

            target = container.childNodes.item(i);
        }
    }

    if (target == null)
    {

    }

    let link = document.createElement("a");
    link.setAttribute("href", "#");
    link.setAttribute("onclick", "LoadAddControl(" + nodeParentId + ", " + entryId + ", " + idTypeSource + ", " + idTypeTarget + "); return false;");
    link.appendChild(document.createTextNode("Add"));
    target.appendChild(link);

    return 0;
}

function ToggleRevisions(id)
{
    let revisionsDiv = document.getElementById(id);

    if (revisionsDiv == null)
    {
        return -1;
    }

    if (revisionsDiv.style.display === "block")
    {
        revisionsDiv.style.display = "none";
    }
    else
    {
        revisionsDiv.style.display = "block";
    }

    return 0;
}

// Stupid JavaScript has a cloneNode(deep), but no removeNode(deep).
function removeNode(element, parent)
{
    // TODO: Reverse delete for performance.
    while (element.hasChildNodes() == true)
    {
        removeNode(element.lastChild, element);
    }

    parent.removeChild(element);
}
