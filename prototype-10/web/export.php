<?php
/* Copyright (C) 2012-2024 Stephan Kreutzer
 *
 * This file is part of GBA.
 *
 * GBA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * GBA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with GBA. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/export.php
 * @author Stephan Kreutzer
 * @since 2024-01-05
 */


require_once("./libraries/https.inc.php");
require_once("./license.inc.php");

if (isset($_GET["format"]) !== true)
{
    http_response_code(400);
    exit(0);
}

if ($_GET["format"] != "json" &&
    $_GET["format"] != "markdown")
{
    http_response_code(400);
    exit(0);
}

$typeSourceId = null;
$typeTargetId = null;

if (isset($_GET["type-source-id"]) === true)
{
    $typeSourceId = (int)$_GET["type-source-id"];
}

if (isset($_GET["type-target-id"]) === true)
{
    $typeTargetId = (int)$_GET["type-target-id"];
}

if ($typeSourceId === null &&
    $typeTargetId === null)
{
    http_response_code(400);
    exit(1);
}

if ($typeTargetId !== null &&
    $typeSourceId === null)
{
    $typeSourceId = $typeTargetId;
    $typeTargetId = null;
}

if ($typeSourceId !== null &&
    $typeTargetId !== null)
{
    if ($typeSourceId == $typeTargetId)
    {
        $typeTargetId = null;
    }
}

require_once("./libraries/database.inc.php");

if (Database::Get()->IsConnected() !== true)
{
    http_response_code(500);
    exit(-1);
}


$entriesSource = null;

if ($typeSourceId !== null)
{
    $entriesSource = Database::Get()->Query("SELECT `".Database::Get()->GetPrefix()."node`.`id` AS `node_id`,\n".
                                            "    `".Database::Get()->GetPrefix()."node`.`id_type` AS `node_id_type`,\n".
                                            "    `".Database::Get()->GetPrefix()."node`.`ordinal` AS `node_ordinal`,\n".
                                            "    `".Database::Get()->GetPrefix()."node_revision`.`id` AS `node_revision_id`,\n".
                                            "    `".Database::Get()->GetPrefix()."node_revision`.`text` AS `node_revision_text`,\n".
                                            "    `".Database::Get()->GetPrefix()."node_revision`.`revision_datetime` AS `node_revision_revision_datetime`,\n".
                                            "    `".Database::Get()->GetPrefix()."node_revision`.`id_user` AS `node_revision_id_user`,\n".
                                            "    `".Database::Get()->GetPrefix()."user`.`name` AS `user_name`\n".
                                            "FROM `".Database::Get()->GetPrefix()."node_revision`\n".
                                            "INNER JOIN `".Database::Get()->GetPrefix()."node` ON\n".
                                            "    `".Database::Get()->GetPrefix()."node`.`id` =\n".
                                            "    `".Database::Get()->GetPrefix()."node_revision`.`id_node`\n".
                                            "INNER JOIN `".Database::Get()->GetPrefix()."user` ON\n".
                                            "    `".Database::Get()->GetPrefix()."user`.`id` =\n".
                                            "    `".Database::Get()->GetPrefix()."node_revision`.`id_user`\n".
                                            "WHERE `".Database::Get()->GetPrefix()."node`.`id_type`=?\n".
                                            "ORDER BY `".Database::Get()->GetPrefix()."node`.`ordinal` ASC,\n".
                                            "    `".Database::Get()->GetPrefix()."node`.`id` ASC,\n".
                                            "    `".Database::Get()->GetPrefix()."node_revision`.`revision_datetime` DESC",
                                            array($typeSourceId),
                                            array(Database::TYPE_INT));

    if (is_array($entriesSource) !== true)
    {
        Database::Get()->RollbackTransaction();
        http_response_code(500);
        exit(-1);
    }
}

$entriesTarget = null;

if ($typeTargetId !== null)
{
    $entriesTarget = Database::Get()->Query("SELECT `".Database::Get()->GetPrefix()."node`.`id` AS `node_id`,\n".
                                            "    `".Database::Get()->GetPrefix()."node`.`id_type` AS `node_id_type`,\n".
                                            "    `".Database::Get()->GetPrefix()."node`.`ordinal` AS `node_ordinal`,\n".
                                            "    `".Database::Get()->GetPrefix()."node_revision`.`id` AS `node_revision_id`,\n".
                                            "    `".Database::Get()->GetPrefix()."node_revision`.`text` AS `node_revision_text`,\n".
                                            "    `".Database::Get()->GetPrefix()."node_revision`.`revision_datetime` AS `node_revision_revision_datetime`,\n".
                                            "    `".Database::Get()->GetPrefix()."node_revision`.`id_user` AS `node_revision_id_user`,\n".
                                            "    `".Database::Get()->GetPrefix()."user`.`name` AS `user_name`\n".
                                            "FROM `".Database::Get()->GetPrefix()."node_revision`\n".
                                            "INNER JOIN `".Database::Get()->GetPrefix()."node` ON\n".
                                            "    `".Database::Get()->GetPrefix()."node`.`id` =\n".
                                            "    `".Database::Get()->GetPrefix()."node_revision`.`id_node`\n".
                                            "INNER JOIN `".Database::Get()->GetPrefix()."user` ON\n".
                                            "    `".Database::Get()->GetPrefix()."user`.`id` =\n".
                                            "    `".Database::Get()->GetPrefix()."node_revision`.`id_user`\n".
                                            "WHERE `".Database::Get()->GetPrefix()."node`.`id_type`=?\n".
                                            "ORDER BY `".Database::Get()->GetPrefix()."node`.`ordinal` ASC,\n".
                                            "    `".Database::Get()->GetPrefix()."node`.`id` ASC,\n".
                                            "    `".Database::Get()->GetPrefix()."node_revision`.`revision_datetime` DESC",
                                            array($typeTargetId),
                                            array(Database::TYPE_INT));

    if (is_array($entriesTarget) !== true)
    {
        Database::Get()->RollbackTransaction();
        http_response_code(500);
        exit(-1);
    }
}

$edges = null;

if ($typeSourceId !== null &&
    $typeTargetId !== null)
{
    $edges = Database::Get()->Query("SELECT `id`,\n".
                                    "    `id_node_source`,\n".
                                    "    `id_type_source`,\n".
                                    "    `id_node_target`,\n".
                                    "    `id_type_target`,\n".
                                    "    `label`\n".
                                    "FROM `".Database::Get()->GetPrefix()."edge`\n".
                                    "WHERE (`id_type_source`=? AND\n".
                                    "     `id_type_target`=?) OR\n".
                                    "    (`id_type_source`=? AND\n".
                                    "     `id_type_target`=?)",
                                    array($typeSourceId, $typeTargetId, $typeTargetId, $typeSourceId),
                                    array(Database::TYPE_INT, Database::TYPE_INT, Database::TYPE_INT, Database::TYPE_INT));

    if (is_array($edges) !== true)
    {
        Database::Get()->RollbackTransaction();
        http_response_code(500);
        exit(-1);
    }
}

$tree = array();

for ($i = 0, $max = count($entriesSource); $i < $max; $i++)
{
    $sourceId = (int)$entriesSource[$i]["node_id"];

    // Continue? As revisions may repeat the node?
    if (array_key_exists($sourceId, $tree) == true)
    {

    }

    $tree[$sourceId] = array();
}

if ($edges != null)
{
    for ($i = 0, $max = count($edges); $i < $max; $i++)
    {
        $edgeSourceId = (int)$edges[$i]["id_node_source"];
        $edgeTargetId = (int)$edges[$i]["id_node_target"];

        if ($edgeSourceId == $edgeTargetId)
        {

        }

        // Reverse connection is implicit for each edge.

        if (array_key_exists($edgeSourceId, $tree) == true)
        {
            if (isset($tree[$edgeSourceId][$edgeTargetId]) == true)
            {
                http_response_code(500);
                exit(-1);
            }

            $tree[$edgeSourceId][$edgeTargetId] = array("id" => $edgeTargetId,
                                                        "label" => $edges[$i]["label"]);
        }

        if (array_key_exists($edgeTargetId, $tree) == true)
        {
            if (isset($tree[$edgeTargetId][$edgeSourceId]) == true)
            {
                http_response_code(500);
                exit(-1);
            }

            $tree[$edgeTargetId][$edgeSourceId] = array("id" => $edgeSourceId,
                                                        "label" => $edges[$i]["label"]);
        }
    }
}



if ($_GET["format"] == "json")
{
    function echoTargetList($sourceId,
                            &$children,
                            &$entriesTarget)
    {
        $max = count($children);

        if ($max <= 0)
        {
            return 1;
        }

        $childrenIds = array_keys($children);

        $lastEntryId = -1;
        $authors = array();
        $deletedId = -1;
        $entryOutputCount = 0;

        foreach ($entriesTarget as $entry)
        {
            $entryId = (int)$entry['node_id'];

            if (in_array($entryId, $childrenIds) != true)
            {
                continue;
            }

            if ($deletedId == $entryId)
            {
                continue;
            }

            if ($lastEntryId == $entryId)
            {
                if (in_array($entry["user_name"], $authors) != true)
                {
                    $authors[] = $entry["user_name"];
                }

                continue;
            }
            else
            {
                $lastEntryId = $entryId;

                if (count($authors) > 0)
                {
                    echo ",\"authors\":[";

                    for ($j = count($authors) - 1; $j >= 0; $j--)
                    {
                        echo json_encode($authors[$j]);

                        if ($j > 0)
                        {
                            echo ",";
                        }
                    }

                    echo "]";

                    $authors = array();
                }

                $authors = array();
                $deleted = strlen($entry['node_revision_text']) <= 0;

                if ($deleted === true)
                {
                    $deletedId = $entryId;
                    continue;
                }

                if ($entryOutputCount > 0)
                {
                    echo "},";
                }

                $authors[] = $entry["user_name"];
                $entryOutputCount += 1;


                echo "{\"text\":".json_encode($entry["node_revision_text"]).",".
                      "\"last_revision_datetime\":".json_encode(str_replace(" ", "T", $entry["node_revision_revision_datetime"])."Z");

                if ($children[$entryId]["label"] != null)
                {
                    echo ",\"label\":".json_encode($children[$entryId]["label"]);
                }
            }
        }

        if (count($authors) > 0 &&
            $deletedId != $lastEntryId)
        {
            echo ",\"authors\":[";

            for ($j = count($authors) - 1; $j >= 0; $j--)
            {
                echo json_encode($authors[$j]);

                if ($j > 0)
                {
                    echo ",";
                }
            }

            echo "]";
        }

        if ($entryOutputCount > 0)
        {
            echo "}";
        }
    }


    header("Content-Type: application/json");

    echo "{\"copyright_license\":".json_encode(getContentLicenseUrl()).",".
          "\"entries\":[";

    if (count($entriesSource) > 0)
    {
        $lastEntryId = -1;
        $authors = array();
        $deletedId = -1;
        $entryOutputCount = 0;

        foreach ($entriesSource as $entry)
        {
            $entryId = (int)$entry['node_id'];

            if ($deletedId == $entryId)
            {
                continue;
            }

            if ($lastEntryId == $entryId)
            {
                if (in_array($entry["user_name"], $authors) != true)
                {
                    $authors[] = $entry["user_name"];
                }

                continue;
            }
            else
            {
                $lastEntryId = $entryId;

                if (count($authors) > 0)
                {
                    echo ",\"authors\":[";

                    for ($j = count($authors) - 1; $j >= 0; $j--)
                    {
                        echo json_encode($authors[$j]);

                        if ($j > 0)
                        {
                            echo ",";
                        }
                    }

                    echo "]";

                    $authors = array();
                }

                $authors = array();
                $deleted = strlen($entry['node_revision_text']) <= 0;

                if ($deleted === true)
                {
                    $deletedId = $entryId;
                    continue;
                }

                if ($entryOutputCount > 0)
                {
                    echo "},";
                }

                $authors[] = $entry["user_name"];
                $entryOutputCount += 1;

                echo "{\"text\":".json_encode($entry["node_revision_text"]).",".
                      "\"last_revision_datetime\":".json_encode(str_replace(" ", "T", $entry["node_revision_revision_datetime"])."Z");

                if ($typeTargetId !== null)
                {
                    if (array_key_exists((int)$entry["node_id"], $tree) == true)
                    {
                        echo ",\"entries\":[";
                        echoTargetList($lastEntryId,
                                       $tree[$lastEntryId],
                                       $entriesTarget);
                        echo "]";
                    }
                }
            }
        }

        if (count($authors) > 0 &&
            $deletedId != $lastEntryId)
        {
            echo ",\"authors\":[";

            for ($j = count($authors) - 1; $j >= 0; $j--)
            {
                echo json_encode($authors[$j]);

                if ($j > 0)
                {
                    echo ",";
                }
            }

            echo "]";
        }

        if ($entryOutputCount > 0)
        {
            echo "}";
        }
    }

    echo "]}";
}
else if ($_GET["format"] == "markdown")
{
    function markdown_encode($string)
    {
        $result = "";

        for ($i = 0, $max = strlen($string); $i < $max; $i++)
        {
            // Some of these maybe only at the start of a line,
            // some only conditionally.
            switch ($string[$i])
            {
            case '\\':
            case '`':
            case '*':
            case '_':
            case '{':
            case '}':
            case '[':
            case ']':
            case '<':
            case '>':
            case '(':
            case ')':
            case '#':
            case '+':
            case '-':
            // To escape a literal number followed by a dot inside an
            // unordered list which shouldn't become an ordered list item.
            //case '.':
            case '!':
            case '|':
                $result .= "\\".$string[$i];
                break;
            default:
                $result .= $string[$i];
                break;
            }
        }

        return $result;
    }

    function echoTargetList($sourceId,
                            &$children,
                            &$entriesTarget)
    {
        $max = count($children);

        if ($max <= 0)
        {
            return 1;
        }

        $childrenIds = array_keys($children);

        $lastEntryId = -1;
        $deletedId = -1;

        foreach ($entriesTarget as $entry)
        {
            $entryId = (int)$entry['node_id'];

            if (in_array($entryId, $childrenIds) != true)
            {
                continue;
            }

            if ($deletedId == $entryId)
            {
                continue;
            }

            if ($lastEntryId == $entryId)
            {
                continue;
            }
            else
            {
                $lastEntryId = $entryId;

                $deleted = strlen($entry['node_revision_text']) <= 0;

                if ($deleted === true)
                {
                    $deletedId = $entryId;
                    continue;
                }

                echo "    -";

                if ($children[$entryId]["label"] != null)
                {
                    echo " ".markdown_encode($children[$entryId]["label"]);
                }

                echo " ".markdown_encode($entry["node_revision_text"])."\n";
            }
        }
    }

    function echoCredits(&$entriesSource, &$entriesTarget, &$tree)
    {
        if (empty($entriesSource) == true)
        {
            return null;
        }

        $iso8601Pattern = DateTimeInterface::ISO8601;

        if (version_compare(PHP_VERSION, "8.2") >= 0)
        {
            $iso8601Pattern = DateTimeInterface::ISO8601_EXPANDED;
        }

        $contributors = array();
        $years = array();
        $fallbackYear = (int)date("Y");

        $lastEntryId = -1;

        for ($i = count($entriesSource) - 1; $i >= 0; $i--)
        {
            $contributors[$entriesSource[$i]["user_name"]] = true;

            {
                $yearInFormat = str_replace(" ", "T", $entriesSource[$i]["node_revision_revision_datetime"])."Z";
                $year = DateTime::createFromFormat($iso8601Pattern, $yearInFormat)->format("o");

                if ($year !== false)
                {
                    $year = (int)$year;

                    if (isset($years[$year]) == true)
                    {
                        if ($years[$year] != false)
                        {
                            $years[$year] = true;
                        }
                    }
                    else
                    {
                        $years[$year] = true;
                    }
                }
                else
                {
                    $years[$fallbackYear] = false;
                }
            }

            if ((int)$entriesSource[$i]["node_id"] != $lastEntryId)
            {
                if (array_key_exists((int)$entriesSource[$i]["node_id"], $tree) == true)
                {
                    $children = $tree[(int)$entriesSource[$i]["node_id"]];

                    foreach (array_reverse($children) as $child)
                    {
                        // OK, all of this is ugly, only there because there's no
                        // node by ID lookup (at the expense of memory), but point
                        // being, to get away with it for the time being, quick and dirty.

                        $targetId = null;

                        for ($k = count($entriesTarget) - 1; $k >= 0; $k--)
                        {
                            if ((int)$entriesTarget[$k]["node_id"] == $child["id"])
                            {
                                $targetId = (int)$entriesTarget[$k]["node_id"];


                                $contributors[$entriesTarget[$k]["user_name"]] = true;

                                $yearInFormat = str_replace(" ", "T", $entriesTarget[$k]["node_revision_revision_datetime"])."Z";
                                $year = DateTime::createFromFormat($iso8601Pattern, $yearInFormat)->format("o");

                                if ($year !== false)
                                {
                                    $year = (int)$year;

                                    if (isset($years[$year]) == true)
                                    {
                                        if ($years[$year] != false)
                                        {
                                            $years[$year] = true;
                                        }
                                    }
                                    else
                                    {
                                        $years[$year] = true;
                                    }
                                }
                                else
                                {
                                    $years[$fallbackYear] = false;
                                }
                            }
                            else
                            {
                                if ($targetId != null)
                                {
                                    // No need to record the same target entry twice.
                                    break;
                                }
                            }
                        }
                    }
                }

                $lastEntryId = (int)$entriesSource[$i]["node_id"];
            }
        }

        ksort($years);

        {
            $yearsIndexed = array();
            $correct = true;

            if (count($years) > 0)
            {
                foreach ($years as $year => $correct)
                {
                    $yearsIndexed[] = array("year" => $year,
                                            "correct" => $correct);

                    if ($correct != true)
                    {
                        $correct = false;
                    }
                }
            }

            $max = count($yearsIndexed);

            if ($max >= 2)
            {
                echo $yearsIndexed[0]["year"]."-".$yearsIndexed[$max-1]["year"];
            }
            else
            {
                echo $yearsIndexed[0]["year"];
            }

            echo " ";

            if ($correct != true)
            {
                echo "\\(?\\) ";
            }
        }


        $contributors = array_keys($contributors);

        for ($i = 0, $max = count($contributors); $i < $max; $i++)
        {
            if ($i > 0)
            {
                echo ", ";
            }

            echo markdown_encode($contributors[$i]);
        }
    }


    header("Content-Type: text/markdown");

    echo "# Entries\n\n";

    if (count($entriesSource) > 0)
    {
        $lastEntryId = -1;
        $deletedId = -1;

        foreach ($entriesSource as $entry)
        {
            $entryId = (int)$entry['node_id'];

            if ($deletedId == $entryId)
            {
                continue;
            }

            if ($lastEntryId == $entryId)
            {
                continue;
            }
            else
            {
                $lastEntryId = $entryId;

                $deleted = strlen($entry['node_revision_text']) <= 0;

                if ($deleted === true)
                {
                    $deletedId = $entryId;
                    continue;
                }

                echo "- ".markdown_encode($entry["node_revision_text"])."\n";

                if ($typeTargetId !== null)
                {
                    if (array_key_exists((int)$entry["node_id"], $tree) == true)
                    {
                        echoTargetList($lastEntryId,
                                       $tree[$lastEntryId],
                                       $entriesTarget);
                    }
                }
            }
        }
    }

    echo "\n# Copyright\n\n".
    "Copyright \\(C\\) ";

    echoCredits($entriesSource, $entriesTarget, $tree);

    // TODO: Not sure if URLs need to be Markdown-encoded, or if it's an
    // option to URL-encode, or assume/demand getContentLicenseUrl() provides
    // URLs in encoded form, as some reserved/special characters in URLs are
    // also Markdown reserved/special characters, however Markdown might be fine
    // with these if inside a Markdown link's URL part?
    echo " – [".markdown_encode(getContentLicenseName())."](".getContentLicenseUrl().").\n\n";
}
else
{

}


?>
