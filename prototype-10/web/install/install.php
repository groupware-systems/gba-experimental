<?php
/* Copyright (C) 2013-2023 Christian Huke, Stephan Kreutzer
 *
 * This file is part of GBA.
 *
 * GBA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * GBA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with GBA. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/install/install.php
 * @brief Installation routine to set up the system.
 * @author Christian Huke, Stephan Kreutzer
 * @since 2013-09-13
 */



require_once("../libraries/https.inc.php");



echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n".
     "<!DOCTYPE html\n".
     "    PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\"\n".
     "    \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n".
     "<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"en\" lang=\"en\">\n".
     "  <head>\n".
     "    <title>Install</title>\n".
     "    <meta http-equiv=\"content-type\" content=\"application/xhtml+xml; charset=UTF-8\"/>\n".
     "  </head>\n".
     "  <body>\n";


$step = 0;

if (isset($_POST['step']) === true)
{
    if (is_numeric($_POST['step']) === true)
    {
        $step = (int)$_POST['step'];

/*
        if ($step == 3 &&
            isset($_POST['retry']) === true)
        {
            // Special handling for step 2 (retry other database connection
            // settings after one connection was already established successfully).
            $step = 2;
        }
*/

        if ($step == 4 &&
            isset($_POST['init']) === true)
        {
            // Special handling for step 3 (redo database initialization after
            // initialization was already completed successfully).
            $step = 3;
        }
        else if ($step == 5 &&
                 isset($_POST['save']) === true)
        {
            // Special handling for step 4 (redo save of the configuration after
            // saving was already completed successfully).
            $step = 4;
        }
    }
}

if (isset($_GET['stepjump']) === true)
{
    if (is_numeric($_GET['stepjump']) === true)
    {
        $step = (int)$_GET['stepjump'];
    }
}


if ($step == 0)
{
    echo "    <div>\n".
         "      <h1>Installation</h1>\n".
         "      <form action=\"install.php\" method=\"post\">\n".
         "        <fieldset>\n".
         "          <input type=\"hidden\" name=\"step\" value=\"1\"/>\n".
         "          <input type=\"submit\" value=\"Continue\"/>\n".
         "        </fieldset>\n".
         "      </form>\n".
         "    </div>\n";
}
else if ($step == 1)
{
    echo "    <div>\n".
         "      <h1>License Agreement</h1>\n";

    require_once("../license.inc.php");
    echo getHTMLLicenseNotification();
    echo "<hr/>\n";
    echo getHTMLLicenseFull();

    echo "      <div>\n".
         "        <form action=\"install.php\" method=\"post\">\n".
         "          <fieldset>\n".
         "            <input type=\"hidden\" name=\"step\" value=\"2\"/>\n".
         "            <input type=\"submit\" value=\"Agree\"/>\n".
         "          </fieldset>\n".
         "        </form>\n".
         "      </div>\n".
         "    </div>\n";
}
else if ($step == 2)
{
    $host = "localhost";
    $username = "gbauser";
    $password = "";
    $database = "gba";
    $prefix = "";

    if (isset($_POST['host']) === true)
    {
        $host = $_POST['host'];
    }

    if (isset($_POST['username']) === true)
    {
        $username = $_POST['username'];
    }

    if (isset($_POST['password']) === true)
    {
        $password = $_POST['password'];
    }

    if (isset($_POST['database']) === true)
    {
        $database = $_POST['database'];
    }

    if (isset($_POST['prefix']) === true)
    {
        $prefix = $_POST['prefix'];
    }

    echo "    <div>\n".
         "      <h1>Database Settings</h1>\n".
         "      <p>\n".
         "        This software requires a running MySQL database server. Please fill in the database connection settings. They'll get written into the file <tt>\$/libraries/database_connect.inc.php</tt> of the software installation directory.\n".
         "      </p>\n";

    if (file_exists("../libraries/database_connect.inc.php") !== true)
    {
        $file = @fopen("../libraries/database_connect.inc.php", "w");

        if ($file != false)
        {
            @fclose($file);
        }
        else
        {
            echo "      <p>\n".
                 "        <span class=\"error\">The attempt to create <tt>\$/libraries/database_connect.inc.php</tt> failed!</span>\n".
                 "      </p>\n";
        }
    }

    if (is_writable("../libraries/database_connect.inc.php") === true)
    {
        echo "      <p>\n".
             "        <span class=\"success\"><tt>\$/libraries/database_connect.inc.php</tt> is writable!</span>\n".
             "      </p>\n";

        /**
         * @todo Make sure that strings in $host, $database, ... don't contain characters
         *     that break PHP or SQL.
         */

        $php_code = "<?php\n".
                    "// This file was automatically generated by the installation routine.\n".
                    "\n".
                    "\$pdo = false;\n".
                    "\$db_table_prefix = \"$prefix\"; // Prefix for database tables.\n".
                    "\$exceptionConnectFailure = NULL;\n".
                    "\n".
                    "\n".
                    "try\n".
                    "{\n".
                    "    \$pdo = @new PDO('mysql:host=".$host.";dbname=".$database.";charset=utf8', \"".$username."\", \"".$password."\", array(PDO::MYSQL_ATTR_INIT_COMMAND => \"SET NAMES utf8\"));\n".
                    "}\n".
                    "catch (PDOException \$ex)\n".
                    "{\n".
                    "    \$pdo = false;\n".
                    "    \$exceptionConnectFailure = \$ex;\n".
                    "}\n".
                    "\n".
                    "?>\n";

        $file = @fopen("../libraries/database_connect.inc.php", "wb");

        if ($file != false)
        {
            if (@fwrite($file, $php_code) != false)
            {
                echo "      <p>\n".
                     "        <span class=\"success\">Writing <tt>\$/libraries/database_connect.inc.php</tt> succeeded!</span>\n".
                     "      </p>\n";
            }
            else
            {
                echo "      <p>\n".
                     "        <span class=\"error\"><tt>\$/libraries/database_connect.inc.php</tt> looked to be writable, was successfully opened, but can't be actually written!</span>\n".
                     "      </p>\n";
            }

            @fclose($file);
        }
        else
        {
            echo "      <p>\n".
                 "        <span class=\"error\"><tt>\$/libraries/database_connect.inc.php</tt> looked to be writable, but opening the file failed!</span>\n".
                 "      </p>\n";
        }
    }
    else
    {
        echo "      <p>\n".
             "        <span class=\"error\"><tt>\$/libraries/database_connect.inc.php</tt> isn't writable! Either there are no write permissions granted for the directory <tt>\$/libraries/</tt>, so that the file can't be created, or the file <tt>\$/libraries/database_connect.inc.php</tt> does already exist, but permissions for writing the file aren't sufficient. You might have to enable write permission manually on the server for the directory <tt>\$/libraries/</tt> and/or for the file <tt>\$/libraries/database_connect.inc.php</tt> (if already existing) with a FTP program (CHMOD command) or via remote connection. Don't forget to reset the permissions after the installation is completed.</span>\n".
             "      </p>\n";
    }


    $successConnect = false;

    clearstatcache();

    if (file_exists("../libraries/database_connect.inc.php") === true)
    {
        if (is_readable("../libraries/database_connect.inc.php") === true)
        {
            echo "      <p>\n".
                 "        <span class=\"success\">Written <tt>\$/libraries/database_connect.inc.php</tt> is readable!</span>\n".
                 "      </p>\n";

            require_once("../libraries/database.inc.php");

            if (Database::Get()->IsConnected() === true)
            {
                $successConnect = true;

                echo "      <p>\n".
                     "        <span class=\"success\">Connection to the database was successfully established!</span>\n".
                     "      </p>\n";
            }
            else
            {
                if (strlen(Database::Get()->GetLastErrorMessage()) > 0)
                {
                    echo "      <p>\n".
                         "        <span class=\"error\">Connection to the database couldn't be established. Error description: ".htmlspecialchars(Database::Get()->GetLastErrorMessage(), ENT_XHTML, "UTF-8")."</span>\n".
                         "      </p>\n";
                }
                else
                {
                    echo "      <p>\n".
                         "        <span class=\"error\">Connection to the database couldn't be established. Error description: No error details!</span>\n".
                         "      </p>\n";
                }
            }
        }
        else
        {
            echo "      <p>\n".
                 "        <span class=\"error\"><tt>\$/libraries/database_connect.inc.php</tt> isn't readable! Either there are no read permissions granted for the directory <tt>\$/libraries/</tt>, so that the file can't be accessed, or the file <tt>\$/libraries/database_connect.inc.php</tt> itself lacks read permission. You might have to enable read permission manually on the server for the directory <tt>\$/libraries/</tt> and/or for the file <tt>\$/libraries/database_connect.inc.php</tt> with a FTP program (CHMOD command) or another file transfer program. Don't forget to reset the permissions after the installation is completed.</span>\n".
                 "      </p>\n";
        }
    }
    else
    {
        echo "      <p>\n".
             "        <span class=\"error\"><tt>\$/libraries/database_connect.inc.php</tt> doesn't exist!</span>\n".
             "      </p>\n";
    }

    if (isset($_POST['save']) == false ||
        $successConnect == false)
    {
        echo "      <div>\n".
             "        <form action=\"install.php\" method=\"post\">\n".
             "          <fieldset>\n".
             "            <input type=\"hidden\" name=\"step\" value=\"2\"/>\n".
             "            <input type=\"text\" name=\"host\" value=\"".htmlspecialchars($host, ENT_XHTML | ENT_QUOTES, "UTF-8")."\"/> Address of the database server<br/>\n".
             "            <input type=\"text\" name=\"username\" value=\"".htmlspecialchars($username, ENT_XHTML | ENT_QUOTES, "UTF-8")."\"/> Database username<br/>\n".
             "            <input type=\"password\" name=\"password\" value=\"".htmlspecialchars($password, ENT_XHTML | ENT_QUOTES, "UTF-8")."\"/> Password of that database user<br/>\n".
             "            <input type=\"text\" name=\"database\" value=\"".htmlspecialchars($database, ENT_XHTML | ENT_QUOTES, "UTF-8")."\"/> Name of the database which will hold the tables<br/>\n".
             "            <input type=\"text\" name=\"prefix\" value=\"".htmlspecialchars($prefix, ENT_XHTML | ENT_QUOTES, "UTF-8")."\"/> Prefix for table names (could be empty if no name collusions are expected – prefix ends usually with an underscore '_')<br/>\n".
             "            <input type=\"submit\" name=\"save\" value=\"Save settings\"/>\n".
             "          </fieldset>\n".
             "        </form>\n".
             "      </div>\n";
    }
    else
    {
        echo "      <div>\n".
             "        <fieldset>\n".
             "          <form action=\"install.php\" method=\"post\">\n".
             "            <input type=\"hidden\" name=\"step\" value=\"2\"/>\n".
             "            <input type=\"hidden\" name=\"host\" value=\"".htmlspecialchars($host, ENT_XHTML | ENT_QUOTES, "UTF-8")."\"/>\n".
             "            <input type=\"hidden\" name=\"username\" value=\"".htmlspecialchars($username, ENT_XHTML | ENT_QUOTES, "UTF-8")."\"/>\n".
             "            <input type=\"hidden\" name=\"password\" value=\"".htmlspecialchars($password, ENT_XHTML | ENT_QUOTES, "UTF-8")."\"/>\n".
             "            <input type=\"hidden\" name=\"database\" value=\"".htmlspecialchars($database, ENT_XHTML | ENT_QUOTES, "UTF-8")."\"/>\n".
             "            <input type=\"hidden\" name=\"prefix\" value=\"".htmlspecialchars($prefix, ENT_XHTML | ENT_QUOTES, "UTF-8")."\"/>\n".
             "            <input type=\"submit\" value=\"Edit settings\"/>\n".
             "          </fieldset>\n".
             "        </form>\n".
             "      </div>\n".
             "      <div>\n".
             "        <form action=\"install.php\" method=\"post\">\n".
             "          <fieldset>\n".
             "            <input type=\"hidden\" name=\"step\" value=\"3\"/>\n".
             "            <input type=\"submit\" value=\"Confirm settings\"/>\n".
             "          </fieldset>\n".
             "        </form>\n".
             "      </div>\n";
    }

    echo "    </div>\n";
}
else if ($step == 3)
{
    $dropExistingTables = false;
    $keepExistingTables = false;

    if (isset($_POST['drop_existing_tables']) === true)
    {
        $dropExistingTables = true;
    }

    if (isset($_POST['keep_existing_tables']) === true)
    {
        $keepExistingTables = true;
    }


    echo "    <div>\n".
         "      <h1>Initialization</h1>\n".
         "      <p>\n".
         "        The software is about to be initialized. This includes the creation of the tables in the database.\n".
         "      </p>\n";


    $successInit = false;

    if (isset($_POST['init']) === true)
    {
        require_once("../libraries/database.inc.php");

        if (Database::Get()->IsConnected() === true)
        {
            $success = true;

            // Table user

            if ($success === true)
            {
                if ($dropExistingTables === true)
                {
                    if (Database::Get()->ExecuteUnsecure("DROP TABLE IF EXISTS `".Database::Get()->GetPrefix()."user`") !== true)
                    {
                        $success = false;
                    }
                }
            }

            if ($success === true)
            {
                $sql = "CREATE TABLE ";

                if ($keepExistingTables === true)
                {
                    $sql .= "IF NOT EXISTS ";
                }

                $sql .= "`".Database::Get()->GetPrefix()."user` (".
                        "  `id` int(11) NOT NULL AUTO_INCREMENT,".
                        "  `name` varchar(40) COLLATE utf8_bin NOT NULL,".
                        "  `e_mail` varchar(255) COLLATE utf8_bin NOT NULL,".
                        "  `salt` varchar(255) COLLATE utf8_bin NOT NULL,".
                        "  `password` varchar(255) COLLATE utf8_bin NOT NULL,".
                        "  `role` int(11) NOT NULL,".
                        "  `last_login` datetime,".
                        "  PRIMARY KEY (`id`),".
                        "  UNIQUE KEY `name` (`name`)".
                        ") ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin";

                if (Database::Get()->ExecuteUnsecure($sql) !== true)
                {
                    $success = false;
                }
            }

            // Table node

            if ($success === true)
            {
                if ($dropExistingTables === true)
                {
                    if (Database::Get()->ExecuteUnsecure("DROP TABLE IF EXISTS `".Database::Get()->GetPrefix()."node`") !== true)
                    {
                        $success = false;
                    }
                }
            }

            if ($success === true)
            {
                $sql = "CREATE TABLE ";

                if ($keepExistingTables === true)
                {
                    $sql .= "IF NOT EXISTS ";
                }

                $sql .= "`".Database::Get()->GetPrefix()."node` (".
                        "  `id` int(11) NOT NULL AUTO_INCREMENT,\n".
                        "  `id_type` int(11) NOT NULL,\n".
                        "  `ordinal` int(11) NOT NULL,\n".
                        "  PRIMARY KEY (`id`)\n".
                        ") ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin";

                if (Database::Get()->ExecuteUnsecure($sql) !== true)
                {
                    $success = false;
                }
            }

            // Table node_revision

            if ($success === true)
            {
                if ($dropExistingTables === true)
                {
                    if (Database::Get()->ExecuteUnsecure("DROP TABLE IF EXISTS `".Database::Get()->GetPrefix()."node_revision`") !== true)
                    {
                        $success = false;
                    }
                }
            }

            if ($success === true)
            {
                $sql = "CREATE TABLE ";

                if ($keepExistingTables === true)
                {
                    $sql .= "IF NOT EXISTS ";
                }

                $sql .= "`".Database::Get()->GetPrefix()."node_revision` (".
                        "  `id` int(11) NOT NULL AUTO_INCREMENT,\n".
                        "  `text` text COLLATE utf8_bin NOT NULL,\n".
                        "  `revision_datetime` DATETIME NOT NULL,\n".
                        "  `id_user` int(11) NOT NULL,\n".
                        "  `id_node` int(11) NOT NULL,\n".
                        "  PRIMARY KEY (`id`)\n".
                        ") ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin";

                if (Database::Get()->ExecuteUnsecure($sql) !== true)
                {
                    $success = false;
                }
            }

            // Table type

            if ($success === true)
            {
                if ($dropExistingTables === true)
                {
                    if (Database::Get()->ExecuteUnsecure("DROP TABLE IF EXISTS `".Database::Get()->GetPrefix()."type`") !== true)
                    {
                        $success = false;
                    }
                }
            }

            if ($success === true)
            {
                $sql = "CREATE TABLE ";

                if ($keepExistingTables === true)
                {
                    $sql .= "IF NOT EXISTS ";
                }

                $sql .= "`".Database::Get()->GetPrefix()."type` (".
                        "  `id` int(11) NOT NULL AUTO_INCREMENT,\n".
                        "  `name` varchar(255) NOT NULL,\n".
                        "  `namespace` text COLLATE utf8_bin,\n".
                        "  PRIMARY KEY (`id`)\n".
                        ") ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin";

                if (Database::Get()->ExecuteUnsecure($sql) !== true)
                {
                    $success = false;
                }
            }

            // Table edge

            if ($success === true)
            {
                if ($dropExistingTables === true)
                {
                    if (Database::Get()->ExecuteUnsecure("DROP TABLE IF EXISTS `".Database::Get()->GetPrefix()."edge`") !== true)
                    {
                        $success = false;
                    }
                }
            }


            if ($success === true)
            {
                $sql = "CREATE TABLE ";

                if ($keepExistingTables === true)
                {
                    $sql .= "IF NOT EXISTS ";
                }

                $sql .= "`".Database::Get()->GetPrefix()."edge` (".
                        "  `id` int(11) NOT NULL AUTO_INCREMENT,\n".
                        "  `id_node_source` int(11) NOT NULL,\n".
                        "  `id_type_source` int(11) NOT NULL,\n".
                        "  `id_node_target` int(11) NOT NULL,\n".
                        "  `id_type_target` int(11) NOT NULL,\n".
                        "  `label` text COLLATE utf8_bin,\n".
                        "  PRIMARY KEY (`id`)\n".
                        ") ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin";

                if (Database::Get()->ExecuteUnsecure($sql) !== true)
                {
                    $success = false;
                }
            }


            if ($success === true)
            {
                $successInit = true;
            }
            else
            {
                if (strlen(Database::Get()->GetLastErrorMessage()) > 0)
                {
                    echo "      <p>\n".
                         "        <span class=\"error\">Database operation failed. Error description: ".htmlspecialchars(Database::Get()->GetLastErrorMessage(), ENT_XHTML, "UTF-8")."</span>\n".
                         "      </p>\n";
                }
                else
                {
                    echo "      <p>\n".
                         "        <span class=\"error\">Database operation failed. Error description: No error details!</span>\n".
                         "      </p>\n";
                }
            }
        }
        else
        {
            if (strlen(Database::Get()->GetLastErrorMessage()) > 0)
            {
                echo "      <p>\n".
                     "        <span class=\"error\">Connection to the database couldn't be established. Error description: ".htmlspecialchars(Database::Get()->GetLastErrorMessage(), ENT_XHTML, "UTF-8")."</span>\n".
                     "      </p>\n";
            }
            else
            {
                echo "      <p>\n".
                     "        <span class=\"error\">Connection to the database couldn't be established. Error description: No error details!</span>\n".
                     "      </p>\n";
            }
        }
    }

    echo "      <div>\n".
         "        <form action=\"install.php\" method=\"post\">\n".
         "          <fieldset>\n";

    if ($successInit === true)
    {
        echo "            <input type=\"hidden\" name=\"step\" value=\"4\"/>\n";
    }
    else
    {
        echo "            <input type=\"hidden\" name=\"step\" value=\"3\"/>\n";
    }

    if ($dropExistingTables === true)
    {
        echo "            <input type=\"checkbox\" name=\"drop_existing_tables\" value=\"drop\" checked=\"checked\"/> Delete tables that already exist (warning: tables can't be recovered!)<br/>\n";
    }
    else
    {
        echo "            <input type=\"checkbox\" name=\"drop_existing_tables\" value=\"drop\"/> Delete tables that already exist (warning: tables can't be recovered!)<br/>\n";
    }

    if ($keepExistingTables === true)
    {
        echo "            <input type=\"checkbox\" name=\"keep_existing_tables\" value=\"keep\" checked=\"checked\"/> Only create tables, if not already existing (will keep existing tables without change)<br/>\n";
    }
    else
    {
        echo "            <input type=\"checkbox\" name=\"keep_existing_tables\" value=\"keep\"/> Only create tables, if not already existing (will keep existing tables without change)<br/>\n";
    }

    echo "            <input type=\"submit\" name=\"init\" value=\"Initialize\"/>\n";

    if ($successInit === true)
    {
        echo "            <input type=\"submit\" value=\"Complete\"/>\n";
    }

    echo "          </fieldset>\n".
         "        </form>\n".
         "      </div>\n".
         "    </div>\n";
}
else if ($step == 4)
{
    $userName = "";
    $userPassword = "";
    $userEMail = "";

    if (isset($_POST['username']) === true)
    {
        $userName = $_POST['username'];
    }

    if (isset($_POST['password']) === true)
    {
        $userPassword = $_POST['password'];
    }

    if (isset($_POST['email']) === true)
    {
        $userEMail = $_POST['email'];
    }

    echo "    <div>\n".
         "      <h1>Configuration</h1>\n".
         "      <div>\n";

    $successCreate = false;

    if (isset($_POST['save']) === true &&
        !empty($userName) &&
        !empty($userPassword) &&
        !empty($userEMail))
    {
        require_once(dirname(__FILE__)."/../libraries/user_management.inc.php");

        $successDelete = false;

        if (Database::Get()->IsConnected() === true)
        {
            $successDelete = Database::Get()->ExecuteUnsecure("DELETE FROM `".Database::Get()->GetPrefix()."user` WHERE 1");
        }

        if ($successDelete === true)
        {
            $id = insertNewUser($userName, $userPassword, $userEMail, USER_ROLE_ADMIN);

            if ($id > 0)
            {
                $user = array("id" => $id);
                $successCreate = true;
            }
        }

        if ($successDelete != true ||
            $successCreate != true)
        {
            echo "        <p>\n".
                 "          <span class=\"error\">Database operation failed.</span>\n".
                 "        </p>\n".
                 "      </div>\n".
                 "    </div>\n".
                 "  </body>\n".
                 "</html>\n";

            exit();
        }
    }

    if ($successCreate == false)
    {
        // If $successCreate !== true because of a failed database operation, the
        // execution will be aborted above. Therefore, this part is only executed
        // if the step 4 is called without $_POST['save'] (= first time).

        echo "        <p>\n".
             "          Please create the first user. This user is also the administrator.\n".
             "        </p>\n";
    }
    else
    {
        echo "        <p>\n".
             "          <span class=\"success\">User successfully created.</span>\n".
             "        </p>\n";
    }

    echo "        <div>\n".
         "          <form action=\"install.php\" method=\"post\">\n".
         "            <fieldset>\n".
         "              <input type=\"hidden\" name=\"step\" value=\"5\"/>\n".
         "              <input type=\"text\" name=\"username\" value=\"".htmlspecialchars($userName, ENT_XHTML | ENT_QUOTES, "UTF-8")."\" size=\"20\" maxlength=\"60\"/> User name<br/>\n".
         "              <input type=\"password\" name=\"password\" value=\"".htmlspecialchars($userPassword, ENT_XHTML | ENT_QUOTES, "UTF-8")."\" size=\"20\" maxlength=\"60\"/> Password<br/>\n".
         "              <input type=\"text\" name=\"email\" value=\"".htmlspecialchars($userEMail, ENT_XHTML | ENT_QUOTES, "UTF-8")."\" size=\"20\" maxlength=\"255\"/> E-mail address<br/>\n".
         "              <input type=\"submit\" name=\"save\" value=\"Save\"/>\n";

    if ($successCreate === true)
    {
        echo "              <input type=\"submit\" value=\"Complete\"/>\n";
    }

    echo "            </fieldset>\n".
         "          </form>\n".
         "        </div>\n".
         "      </div>\n".
         "    </div>\n";
}
else if ($step == 5)
{
    echo "    <div>\n".
         "      <h1>Complete!</h1>\n".
         "      </div>\n".
         "      <p>\n".
         "        Installation succeeded! The installation routine will try to delete itself. If it can't succeed (login on the main page won't be possible then, instead, you'll get forwarded to the installation again), you have to delete the directory <tt>\$/install/</tt> manually, or at least the file <tt>\$/install/install.php</tt>. Then the login form should be accessible.\n".
         "      </p>\n".
         "      <div>\n".
         "        <form action=\"../index.php\" method=\"post\">\n".
         "          <fieldset>\n".
         "            <input type=\"hidden\" name=\"install_done\" value=\"install_done\"/>\n".
         "            <input type=\"submit\" value=\"Exit\"/>\n".
         "          </fieldset>\n".
         "        </form>\n".
         "      </div>\n".
         "    </div>\n";
}

echo "  </body>\n".
     "</html>\n";



?>
